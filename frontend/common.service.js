eventplanner.factory("CommonService", ['$rootScope','$timeout','$mdToast', function ($rootScope, $timeout, $mdToast) {
        var service = {};
        // service.banner = banner;
        service.amigable = amigable;
        service.toaster = toaster;
        return service;

        // function banner(message, type) {
        //     $rootScope.bannerText = message;
        //     $rootScope.bannerClass = 'alertbanner aletbanner' + type;
        //     $rootScope.bannerV = true;
        //
        //     $timeout(function () {
        //         $rootScope.bannerV = false;
        //         $rootScope.bannerText = "";
        //     }, 5000);
        // }

        function amigable(url) {
            var link = "";
            url = url.replace("?", "");
            url = url.split("&");

            for (var i = 0; i < url.length; i++) {
                var aux = url[i].split("=");
                link += aux[1] + "/";
            }
            return link;
        }

        function toaster(content, position, delay){
          console.log(content);
          // console.log(position);
          // console.log(delay);
          // console.log(theme);
          $mdToast.show(
            $mdToast.simple()
              .textContent(content)
              .position(position)
              .hideDelay(delay)
              // .theme()
          );
        }
}]);
