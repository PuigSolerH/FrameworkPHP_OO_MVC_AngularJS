var eventplanner = angular.module('eventplanner',['ngRoute', 'ui.bootstrap','ngMaterial','ngAnimate']);
// var eventplanner = angular.module('eventplanner',['ngRoute', 'ngAnimate', 'ui.bootstrap', 'ngCookies', 'facebook']);
eventplanner.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
                // Home
                // .when("/", {templateUrl: "frontend/modules/main/view/main.view.html", controller: "mainCtrl"})
                .when("/", {
                    templateUrl: "frontend/modules/main/view/main.view.html",
                    controller: "mainCtrl",
                    resolve: {
                        categories: function (services) {
                          return services.get('main', 'list_categories');
                        }
                      }
                    })

                // Contact
                .when("/contact", {templateUrl: "frontend/modules/contact/view/contact.view.html", controller: "contactCtrl"})

                // Players
                .when("/players", {
                    templateUrl: "frontend/modules/players/view/players.view.html",
                    controller: "playersCtrl",
                    resolve: {
                        players: function (services) {
                            return services.get('players', 'all_players');
                        }
                        // ,
                        // players2: function (services, $route) {
                        //   return services.get('players', 'players_autocomplete', $route.current.params.name);
                        // }
                    }
                })

                .when("/players/:id", {
                    templateUrl: "frontend/modules/players/view/details.view.html",
                    controller: "detailsCtrl",
                    resolve: {
                        data: function (services, $route) {
                            return services.get('players', 'player_details', $route.current.params.id);
                        },
                        comments: function (services, $route) {
                            return services.get('players', 'player_comments', $route.current.params.id);
                        }
                    }
                })

                .when("/players/where/:country", {
                    templateUrl: "frontend/modules/players/view/players.view.html",
                    controller: "playersCtrl",
                    resolve: {
                        players: function (services, $route) {
                            return services.get('players', 'players_filtered', $route.current.params.country);
                        }
                    }
                })

                //Login
                .when("/login", { templateUrl: "frontend/modules/login/view/login.view.html", controller: "loginCtrl"})

                //Signup
                .when("/login/signup/", { templateUrl: "frontend/modules/login/view/signup.view.html", controller: "signupCtrl"})

                //Recover
                .when("/login/recover/", { templateUrl: "frontend/modules/login/view/recover.view.html", controller: "recoverCtrl"})

                //Activar Usuario
                .when("/login/verify/:token", {templateUrl: "frontend/modules/main/view/main.view.html", controller: "verifyCtrl" })

                //ChangePass
                .when("/login/password/:token", {templateUrl: "frontend/modules/login/view/password.view.html", controller: "passwordCtrl"})

                //Perfil
                .when("/user/profile/", {
                    templateUrl: "frontend/modules/login/view/profile.view.html",
                    controller: "profileCtrl",
                    resolve: {
                        user: function (services, localStorage) {
                            var user = localStorage.get();
                            // console.log(user.token);
                            if (user) {
                                return services.get('login', 'profile_filler', user.token);
                            }
                            return false;
                        }
                    }
                })

                //Likes
                .when("/liked", {
                    templateUrl: "frontend/modules/players/view/liked.view.html",
                    controller: "playersfavoriteCtrl",
                    resolve: {
                        players: function (services, $route, localStorage ) {
                            var user = localStorage.get();
                            console.log(user);
                            return services.post('players', 'getfavorites', JSON.stringify({usuario: user.user}));
                            // return services.post('players', 'getfavorites', user.user.username);
                        }
                    }
                })

                // else 404
                .otherwise("/", {templateUrl: "frontend/modules/main/view/main.view.html", controller: "mainCtrl"});
    }]);
