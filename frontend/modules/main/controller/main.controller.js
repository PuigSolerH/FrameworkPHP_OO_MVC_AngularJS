eventplanner.controller('mainCtrl', function ($scope, categories, $route) {
    // console.log($scope);
    console.log(categories);
    // console.log($route);
    $scope.category = categories.main;
    console.log($scope.category);
    categories.limit = 2;
    $scope.limit = categories.limit;
    $scope.dis = true;

    $scope.loadMore = function() {
      var increamented = categories.limit + 2;
      // console.log(increamented);
      categories.limit = increamented > $scope.category.length ? $scope.category.length : increamented;
      console.log(categories.limit);
      $scope.limit = categories.limit;
      if ($scope.limit >= $scope.category.length){
          $scope.dis = false;
      }
    }

    $scope.complete = function(string){
      // console.log($scope.text);
      // console.log(players);
        var category = $scope.text;
        console.log(category);
        var output = [];
        angular.forEach($scope.category, function(text){
            output.push(text);
        });
      $scope.filterCategories = output;
    }

    $scope.fillTextbox = function(string){
        console.log(string);
        $scope.text = string;
        // $scope.hidethis = true;
    }

    $scope.images = [{
        src: 'players1.jpg',
        title: 'SOCCER'
      }, {
        src: 'players2.jpg',
        title: 'ICE HOCKEY'
      }, {
        src: 'players3.jpg',
        title: 'BASKETBALL'
      }, {
        src: 'players4.jpg',
        title: 'FOOTBALL'
      }, {
        src: 'players5.jpg',
        title: 'BASEBALL'
    }];
});
