eventplanner.controller('contactCtrl', function ($scope, services, CommonService) {
    $scope.contact = {
        inputName: "",
        inputEmail: "",
        inputSubject: "",
        inputMessage: ""
    };

    $scope.SubmitContact = function () {
        var data = {"inputName": $scope.contact.inputName, "inputEmail": $scope.contact.inputEmail,
        "inputSubject": $scope.contact.inputSubject, "inputMessage": $scope.contact.inputMessage,"token":'contact_form'};
        var contact_form = JSON.stringify(data);
        console.log(contact_form);
        services.post('contact', 'process_contact', contact_form).then(function (response) {
            // console.log(response);
            response = response.split("|");
            // console.log(response);
            $scope.message = response[1];
            console.log($scope.message);
            if (response[1] == 'true') {
              // console.log("HOLA");
                CommonService.toaster("SUCCESS",'top right',5000);
                // $scope.class = 'alert alert-success';
            } else {
                CommonService.toaster("ERROR",'top right',5000);
                // $scope.class = 'alert alert-error';
            }
        });
    };
});
