eventplanner.controller('playersCtrl', function ($scope, players, events_map, $route) {
  console.log(players);
  // alert("HOLA");
  // console.log($scope);
  // console.log(players.players);
  // console.log($route);
  console.log(events_map);
    $scope.filteredPlayers = [];
    $scope.markers = [];
    $scope.player = players.players;
    $scope.numPerPage = 6;
    $scope.maxSize = 5;
    $scope.currentPage = 1;
    $scope.type = $route.current.params.country;
    $scope.mapholderV = false;

    console.log(players.players);
    console.log($route.current.params.country);

    setTimeout(function(){ events_map.cargarmap($scope.player, $scope); }, 2);

  //  // $scope.$watch('currentPage + numPerPage', update);

    $scope.filteredPlayers = $scope.player.slice(0, 3);

	$scope.pageChanged = function() {
	  var startPos = ($scope.currentPage - 1) * 3;
	  $scope.filteredPlayers = $scope.player.slice(startPos, startPos + 3);
	  console.log($scope.currentPage);
	};

  $scope.complete = function(string){
    // console.log($scope.text);
    // console.log(players);
  		var category = $scope.text;
      console.log(category);
  		var output = [];
  		angular.forEach($scope.player, function(text){
  				output.push(text);
  		});
  	$scope.filterPlayers = output;
  }

  $scope.fillTextbox = function(string){
      console.log(string);
  		$scope.text = string;
  		// $scope.hidethis = true;
  }
});

eventplanner.controller('detailsCtrl', function ($scope, data, comments, services, CommonService, events_map, localStorage, $location) {
    // console.log(data);
    // console.log(comments.players);
    var user = localStorage.get('user');
    // console.log(user);

    $scope.data = data.players;
    if (comments.players) {
      $scope.comments = comments.players;
    } else {
      $scope.comments = "";
    }
    // console.log(data.players);
    $scope.filteredComments = [];
    $scope.numPerPage = 6;
    $scope.maxSize = 5;
    $scope.currentPage = 1;
    $scope.markers = [];
    $scope.favoriteV = false;

    if (user){
      $scope.favoriteV = true;
      $scope.comentarioV = false;
    } else {
      $scope.favoriteV = false;
      $scope.comentarioV = true;
    }

    setTimeout(function(){ events_map.cargarmapEvent(data.players, $scope); }, 2);

    $scope.addfavorite = function () {
        var data = {"username": user, "favorite": $scope.data.id}
        var arr = JSON.stringify(data);
        console.log(arr);
        services.post("players", "addfavorites", arr).then(function (response) {
          console.log(response);
          if (response.success) {
            CommonService.toaster("SUCCESS",'top right',3000);
          }
          if (response.error) {
            CommonService.toaster("ERROR",'top right',3000);
          }
          if (response.player) {
            CommonService.toaster("You have already added this player",'top right',3000);
          }
          // console.log(response);
            // $scope.userfavorites = response;
        });
    };

    $scope.filteredComments = $scope.comments.slice(0, 3);

  	$scope.pageChanged = function() {
  	  var startPos = ($scope.currentPage - 1) * 3;
  	  $scope.filteredComments = $scope.comments.slice(startPos, startPos + 3);
  	  console.log($scope.currentPage);
  	};

    $scope.SubmitComment = function () {
        // console.log(user);
        // console.log($scope.data.id);
        // console.log(comentario);
        console.log($scope.comentario.comment.$viewValue);
        var data = {"comment": $scope.comentario.comment.$viewValue, "user": user, "player": $scope.data.id};
        var data_users_JSON = JSON.stringify(data);
        console.log(data_users_JSON);
        services.post('players', 'create_comment', data_users_JSON).then(function (response) {
            console.log(response);
            if (response.success) {
                $location.path('/');
                CommonService.toaster("SUCCESS",'top right',3000);
            } else {
              CommonService.toaster("ERROR",'top right',3000);
            }
        });
    };

});

eventplanner.controller('playersfavoriteCtrl', function($location, $scope, players, events_map, localStorage, services, CommonService) {
console.log(players);
// console.log($scope);

    $scope.filteredPlayers = [];
    $scope.markers = [];
    $scope.player = players;
    $scope.numPerPage = 6;
    $scope.maxSize = 5;
    $scope.currentPage = 1;
    $scope.mapholderV = false;
    setTimeout(function(){ events_map.cargarmap($scope.player, $scope); }, 2);

  //  // $scope.$watch('currentPage + numPerPage', update);

    $scope.filteredPlayers = $scope.player.slice(0, 3);

  	$scope.pageChanged = function() {
  	  var startPos = ($scope.currentPage - 1) * 3;
  	  $scope.filteredPlayers = $scope.player.slice(startPos, startPos + 3);
  	  console.log($scope.currentPage);
  	};

    $scope.complete = function(string){
      // console.log($scope.text);
      // console.log(players);
    		var category = $scope.text;
        console.log(category);
    		var output = [];
    		angular.forEach($scope.player, function(text){
    				output.push(text);
    		});
    	$scope.filterPlayers = output;
    }

    $scope.fillTextbox = function(string){
        console.log(string);
    		$scope.text = string;
    		// $scope.hidethis = true;
    }

    $scope.removefavorite = function () {
      // var user = localStorage.get('user');
      // console.log(user);
      // console.log(this.of.username);
      // console.log(this.of.player);
        var data = {"username": this.of.username, "favorite": this.of.player}
        var arr = JSON.stringify(data);
        console.log(arr);
        services.post("players", "removefavorites", arr).then(function (response) {
          console.log(response);
          if (response.success) {
            $location.path('/');
            CommonService.toaster("SUCCESS",'top right',3000);
          }
          if (response.error) {
            CommonService.toaster("ERROR",'top right',3000);
          }
          // console.log(response);
            // $scope.userfavorites = response;
        });
    };

});
