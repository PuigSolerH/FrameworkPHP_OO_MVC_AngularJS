eventplanner.factory("events_map", ['$rootScope',
function ($rootScope) {
    var service = {};
    service.cargarmap = cargarmap;
    service.cargarmapEvent = cargarmapEvent;
    service.marcar = marcar;
    return service;

    function cargarmap(arrArguments, $rootScope) {
      // console.log(arrArguments.length);
        navigator.geolocation.getCurrentPosition(showPosition, showError);

        function showPosition(position){
          // console.log(position.coords);
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            latlon = new google.maps.LatLng(lat, lon);
            mapholder = document.getElementById('mapholder');
            mapholder.style.height = '500px';
            mapholder.style.width = '1250px';

            var myOptions = {
                center: latlon, zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };
            var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);
            // var marker = new google.maps.Marker({position: latlon, map: map, title: "You are here!"});

            $rootScope.map = map;
            for (var i = 0; i < arrArguments.length; i++) {
                marcar(map, arrArguments[i], $rootScope);
            }
        }

        function showError(error){
            switch (error.code){
                case error.PERMISSION_DENIED:
                    $rootScope.demo = "Denegada la peticion de Geolocalización en el navegador.";
                    break;
                case error.POSITION_UNAVAILABLE:
                    $rootScope.demo = "La información de la localización no esta disponible.";
                    break;
                case error.TIMEOUT:
                    $rootScope.demo = "El tiempo de petición ha expirado.";
                    break;
                case error.UNKNOWN_ERROR:
                    $rootScope.demo = "Ha ocurrido un error desconocido.";
                    break;
            }
        }
    }

    function cargarmapEvent(event, $rootScope) {
      // console.log(event);
        navigator.geolocation.getCurrentPosition(showPosition, showError);

        function showPosition(position){
          // console.log(position);
            lat = event.lat;
            lon = event.lon;
            latlon = new google.maps.LatLng(lat, lon);
            mapholder = document.getElementById('mapholder');
            mapholder.style.height = '250px';
            mapholder.style.width = '400px';

            var myOptions = {
                center: latlon, zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: true
            };
            var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);
            $rootScope.map = map;
            marcar(map, event, $rootScope);
        }

        function showError(error){
            switch (error.code){
                case error.PERMISSION_DENIED:
                    $rootScope.demo = "Denegada la peticion de Geolocalización en el navegador.";
                    break;
                case error.POSITION_UNAVAILABLE:
                    $rootScope.demo = "La información de la localización no esta disponible.";
                    break;
                case error.TIMEOUT:
                    $rootScope.demo = "El tiempo de petición ha expirado.";
                    break;
                case error.UNKNOWN_ERROR:
                    $rootScope.demo = "Ha ocurrido un error desconocido.";
                    break;
            }
        }
    }

    function marcar(map, event, $rootScope) {
      // console.log(event);
        var latlon = new google.maps.LatLng(event.lat, event.lon);
        var marker = new google.maps.Marker({position: latlon, map: map, title: event.descripcion, animation: null});

        marker.set('id', event.id);
        marker.set('latlon', latlon);

        var infowindow = new google.maps.InfoWindow({
          content: '<div class="icon">' +
                    '<img src="'+event.img+'" class="icon-md" height="50" width="50"></img>' +
                  '</div>' +
                  '<h4 class="media-heading">'+event.name+'&nbsp;'+event.surname+'</h4>' +
                  '<h6> Position: '+event.position+'</h6>' +
                  '<a href="#/players/'+event.id+'">Ver mas</a>'
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
        $rootScope.markers.push(marker);
    }

}]);
