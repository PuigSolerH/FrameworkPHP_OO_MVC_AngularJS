eventplanner.factory("UserService", ['$location', '$rootScope','localStorage','services',
function ($location, $rootScope, localStorage, services) {
        var service = {};
        service.login = login;
        service.logout = logout;
        $rootScope.user = ""
        return service;
        // $rootScope.accederV = true;

        function login() {
          var user = localStorage.get('token');
          // console.log(user);

          if (user){
            services.get('login', 'get_data', user).then(function(data){
                // console.log(user);
                console.log(data);
                // console.log(data.user[0].token_get);
                  $rootScope.accederV = false;
                  $rootScope.profileV = true;
                  $rootScope.logoutV = true;

                  // $rootScope.myeventsV = true;
                  $rootScope.likedV = true;

                  $rootScope.avatar = data.user[0].userpic;
                  // console.log(user[0].username);
                  $rootScope.nombre = data.user[0].name;
            });
          }else {
            $rootScope.accederV = true;
          }
        }

        function logout() {
            localStorage.remove('token');
            localStorage.remove('user');

            $rootScope.accederV = true;
            $rootScope.profileV = false;
            // $rootScope.myeventsV = false;
            $rootScope.likedV = false;

            $rootScope.avatar = '';
            $rootScope.nombre = '';

            // $rootScope.adminV = false;
            // $rootScope.misofertasV = false;

            $rootScope.logoutV = false;
            $location.path("/");
        }
}]);
