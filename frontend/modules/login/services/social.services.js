eventplanner.factory("socialService", ['CommonService','services','UserService','localStorage','$location',
function (CommonService, services, UserService, localStorage, $location) {

      var config = {
        apiKey: "AIzaSyAxYofiIxDAmUTRDcagCBRp327xsoy2hE0",
        authDomain: "ennoangularjs.firebaseapp.com",
        databaseURL: "https://ennoangularjs.firebaseio.com",
        projectId: "ennoangularjs",
        storageBucket: "ennoangularjs.appspot.com",
        messagingSenderId: "1089359795111"
      };
      // console.log(config);
      firebase.initializeApp(config);

  var social = {};
  social.fb_data = fb_data;
  social.tw_data = tw_data;
  social.g_data = g_data;
  // social.login_sn = login_sn;
  return social;

  function fb_data(){
      var provider = new firebase.auth.FacebookAuthProvider();
      var authService = firebase.auth();

      authService.signInWithPopup(provider)
          .then(function(result) {
                  var data = {"id": result.user.uid, "nombre": result.user.displayName, "email": result.user.email, "userpic":result.user.photoURL };
                  var datos_social = JSON.stringify(data);
                  console.log(datos_social);
                  login_sn(datos_social);
        })
        .catch(function(error) {
          CommonService.toaster("ERROR",'top right',5000);
          console.log('Detectado un error:', error);
        });
  }

  function tw_data() {
      var provider = new firebase.auth.TwitterAuthProvider();
      var authService = firebase.auth();

      authService.signInWithPopup(provider)
          .then(function(result) {
                  var data = {"id": result.user.uid, "nombre": result.user.displayName, "email": result.user.email, "userpic":result.user.photoURL };
                  var datos_social = JSON.stringify(data);
                  console.log(datos_social);
                  login_sn(datos_social);
        })
        .catch(function(error) {
          CommonService.toaster("ERROR",'top right',5000);
          console.log('Detectado un error:', error);
        });
  }

  function g_data() {
      var provider = new firebase.auth.GoogleAuthProvider();
      var authService = firebase.auth();

      authService.signInWithPopup(provider)
          .then(function(result) {
                  var data = {"id": result.user.providerData[0].uid, "nombre": result.user.displayName, "email": result.user.email, "userpic":result.user.photoURL };
                  var datos_social = JSON.stringify(data);
                  console.log(datos_social);
                  login_sn(datos_social);
        })
        .catch(function(error) {
          CommonService.toaster("ERROR",'top right',5000);
          console.log('Detectado un error:', error);
        });
  }

  function login_sn(datos_social) {
    // console.log(datos_social);
      services.post("login", 'login_sn', {user: datos_social})
      .then(function (response) {
        // console.log(response);
          //console.log(response[0]);
          if (!response.error) {
            // console.log(response);
              // cookiesService.SetCredentials(response[0]);
              localStorage.set('token', response);
              // // $scope.close();
              UserService.login();
              CommonService.toaster("SUCCESS",'top right',5000);
              $location.path("/");
          } else {
              if (response.datos == 503)
                  CommonService.toaster("ERROR",'top right',5000);
          }
      });
  }
}]);
