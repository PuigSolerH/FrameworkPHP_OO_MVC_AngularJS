eventplanner.factory("load_dropdowns", ['services', '$q',
function (services, $q) {
    var service = {};
    service.load_country = load_country;
    service.load_league = load_league;
    service.load_team = load_team;
    return service;

    function load_country() {
        var deferred = $q.defer();
        services.get("login", "load_pais_user", true).then(function (data) {
            // console.log(data);
            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_country" });
            } else {
                deferred.resolve({ success: true, datas: data.paises });
                //$.each(data, function (i, valor) {
                    //console.log(valor.sName);
                    //console.log(valor.sISOCode);
                //});
            }
        });
        return deferred.promise;
    };

    function load_league(datos) {
      // console.log(datos);
        var deferred = $q.defer();
        services.get("login", "load_ligas_user", datos.id).then(function (data) {
            // console.log(data);
            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_provincias" });
            } else {
                deferred.resolve({ success: true, datas: data.ligas });
                //$.each(data.provincias, function (i, valor) {
                    //console.log(valor.id);
                    //console.log(valor.nombre);
                //});
            }
        });
        return deferred.promise;
    };

    function load_team(datos) {
        var deferred = $q.defer();
        services.get("login", "load_equipos_user", datos.id).then(function (data) {
            // console.log(data);
            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_poblaciones" });
            } else {
                deferred.resolve({ success: true, datas: data.equipos });
                //$.each(data.poblaciones, function (i, valor) {
                    //console.log(valor.poblacion);
                //});
            }
        });
        return deferred.promise;
    };
}]);
