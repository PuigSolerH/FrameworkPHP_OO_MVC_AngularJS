eventplanner.factory('localStorage', ['services','$q', function (services,$q) {
      'use strict';

      var set = function(name, data){
        amplify.store(name, data);
      };


      var get = function(name){
        return amplify.store(name);
      };


      var remove = function(name){
        return amplify.store(name, null);
      };

      return{
        set: set,
        get : get,
        remove : remove
      }
  }]);
