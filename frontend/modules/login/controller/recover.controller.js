eventplanner.controller('recoverCtrl', function ($scope, services, CommonService, $location) {
  $scope.restore = {
      inputEmail: ""
  };

  $scope.SubmitRestore = function () {
      // var data = {"inputEmail": $scope.restore.inputEmail, "token": 'restore_form'};
      var data = {"inputEmail": $scope.restore.inputEmail};
      // var restore_form = JSON.stringify(data);
      // console.log(restore_form);
      console.log(data);

      services.post('login', 'restore', data).then(function (response) {
          // console.log(response);
          if (response.error) {
            CommonService.toaster("ERROR",'top right',5000);
              // $scope.class = 'alert alert-success';
              // $timeout(function () {
                  $location.path('/');
              //     CommonService.banner("Revisa la bandeja de tu correo", "");
              // }, 3000);
          } else {
              CommonService.toaster("SUCCESS",'top right',5000);
              $location.path('/');
              // $scope.class = 'alert alert-error';
              // $timeout(function () {
              //     $location.path('/');
              //     CommonService.banner("Intentelo mas tarde...", "");
              // }, 3000);
          }
      });
  };
});
