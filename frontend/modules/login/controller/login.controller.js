eventplanner.controller('loginCtrl', function ($scope, socialService, CommonService, localStorage, UserService, services, $location, $timeout) {
  // alert("HOLA");
  $scope.form = {
      user: "",
      pass: ""
  };

  $scope.loginFb = function(){
    socialService.fb_data();
    // alert("FACEBOOK");
  }

  $scope.loginTw = function(){
    socialService.tw_data();
    // alert("TWITTER");
  }

  $scope.loginG = function(){
    socialService.g_data();
    // alert("GOOGLE+");
  }

  $scope.login = function(){
      var data = {"usuario": $scope.form.user, "pass": $scope.form.pass};
      // data = JSON.stringify(data);
      console.log(data);

      services.post("login", "login_manual", data).then(function (response) {
          //console.log(response);
          //console.log(response[0].usuario);
          if (!response.error) {
              localStorage.set('token', response);
              var usuario = $scope.form.user;
              localStorage.set('user', usuario);
              UserService.login();
              CommonService.toaster("SUCCESS",'top right',5000);
              $location.path("/");
          } else {
              if (response.datos == 503)
                  CommonService.toaster("ERROR",'top right',5000);
              else if (response.datos == 404){
                  $location.path("/");
                  CommonService.toaster("ERROR",'top right',5000);
              }else {
                  $scope.err = true;
                  $scope.errorpass = response.datos;
                  $timeout(function () {
                      $scope.err = false;
                      $scope.errorpass = "";
                  }, 1500);
              }
          }
      });
  }
});
