eventplanner.controller('verifyCtrl', function (UserService, $location, CommonService, $route, services, localStorage) {
    var token = $route.current.params.token;
    console.log(token);
    if (token.substring(0, 3) !== 'Ver') {
        CommonService.toaster("ERROR",'top right',5000);
        $location.path('/');
    }
    services.get("login", "verify", token).then(function (response) {
        //console.log(response);
        //console.log(response.user[0].usuario);
        if (response.success) {
            CommonService.toaster("SUCCESS",'top right',5000);
            localStorage.set(response);
            UserService.login();
            $location.path('/');
        } else {
            if (response.datos == 503){
                CommonService.toaster("ERROR",'top right',5000);
                $location.path("/");
            }else if (response.error == 404){
                CommonService.toaster("ERROR",'top right',5000);
                $location.path("/");
            }
        }
    });
});
