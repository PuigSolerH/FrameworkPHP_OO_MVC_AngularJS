eventplanner.controller('profileCtrl', function ($scope, UserService, services, user, $location, localStorage, load_dropdowns,
$timeout, CommonService) {
    // console.log(user);

    //llenar los campos del form_profile con scope
    $scope.user = user.user;
    $scope.drop = {
        msgClass: ''
    };
    if (!isNaN(user.user.username))
        $scope.user.username = user.user.username;
        // console.log(user.user.username);

    //disabled mail
    $scope.controlmail = false; //ng-disabled=false
    if (user.user.email)
        $scope.controlmail = true;

    //disabled pop type
    $scope.controlTypePop = false; //ng-disabled=false
    if (user.user.typePop)
        $scope.controlTypePop = true;

    //errors
    $scope.error = function() {
        $scope.user.nombre_error = "";
        // $scope.user.surn_error = "";
        // $scope.user.favorites_error = "";
        $scope.user.email_error = "";
        $scope.user.pais_error = "";
        $scope.user.liga_error = "";
        $scope.user.equipo_error = "";
    };
    $scope.change_profile = function () {
        $scope.user.nombre_error = "";
        // $scope.user.surn_error = "";
        // $scope.user.favorites_error = "";
        $scope.user.email_error = "";
    };

    load_dropdowns.load_country()
    .then(function (response) {
      console.log(response);
        if(response.success){
            $scope.paises = response.datas;
        }else{
            $scope.AlertMessage = true;
            $scope.user.pais_error = "Error al recuperar la informacion de paises";
            $timeout(function () {
                $scope.user.pais_error = "";
                $scope.AlertMessage = false;
            }, 2000);
        }
    });
    //$scope.provincias = null; //en ng-disabled
    //$scope.poblaciones = null; //en ng-disabled

    $scope.resetPais = function () {
        if ($scope.user.pais.id) {
            var datos = {id: $scope.user.pais.id};
            // console.log(datos);
            load_dropdowns.load_league(datos)
            .then(function (response) {
              console.log(response);
                if(response.success){
                    $scope.ligas = response.datas;
                }else{
                    $scope.AlertMessage = true;
                    $scope.user.liga_error = "Error al recuperar la informacion de ligas";
                    $timeout(function () {
                        $scope.user.liga_error = "";
                        $scope.AlertMessage = false;
                    }, 2000);
                }
            });
            $scope.poblaciones = null;
        }
         /*else { //en ng-disabled
            $scope.provincias = null;
            $scope.poblaciones = null;
        }*/
    };

    $scope.resetValues = function () {
        var datos = {id: $scope.user.liga.id};
        console.log(datos);
        load_dropdowns.load_team(datos)
        .then(function (response) {
          console.log(response)
            if(response.success){
                $scope.equipos = response.datas;
            }else{
                $scope.AlertMessage = true;
                $scope.user.equipo_error = "Error al recuperar la informacion de equipos";
                $timeout(function () {
                    $scope.user.equipo_error = "";
                    $scope.AlertMessage = false;
                }, 2000);
            }
        });
    };
    //dropzone
    $scope.dropzoneConfig = {
        'options': {
            'url': 'backend/index.php?module=login&function=upload_avatar',
            addRemoveLinks: true,
            maxFileSize: 1000,
            dictResponseError: "Ha ocurrido un error en el server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd'
        },
        'eventHandlers': {
            'sending': function (file, formData, xhr) {},
            'success': function (file, response) {
                console.log(response);
                response = JSON.parse(response);
                //console.log(response);
                if (response.resultado) {
                    $(".msg").addClass('msg_ok').removeClass('msg_error').text('Success Upload image!!');
                    $('.msg').animate({'right': '300px'}, 300);

                    //console.log(response.datos);
                    $scope.user.userpic = response.datos;

                    var user = {username: $scope.user.username, userpic: response.datos, name: $scope.user.name};
                    localStorage.set('user',user);

                    UserService.login();
                } else {
                    $(".msg").addClass('msg_error').removeClass('msg_ok').text(response['error']);
                    $('.msg').animate({'right': '300px'}, 300);
                }
            },
            'removedfile': function (file, serverFileName) {
                if (file.xhr.response) {
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    var data = jQuery.parseJSON(file.xhr.response);
                    services.post("login", "delete_avatar", JSON.stringify({'filename': data}));
                }
            }
        }
    };

            // console.log($scope.user.username);

    $scope.submit = function () {
        var pais, liga, equipo = null;

        if (!$scope.user.pais.id) { //el usuario no escoge provincia
            pais = " ";
            liga = " ";
            equipo = " ";
        }else{ //el usuario escoge provincia
            pais = $scope.user.pais.name;
        }

        if (!$scope.user.pais.id) { //el usuario no escoge pais
            pais = " ";
        }else{ //el usuario escoge pais
            pais = $scope.user.pais.name;
            // if($scope.user.pais.id !== "ES"){
            //     liga = " ";
            //     pob = " ";
            // }
        }

        if (!$scope.user.liga.id) { //el usuario no escoge provincia
            liga = " ";
            equipo = " ";
        }else{ //el usuario escoge provincia
            liga = $scope.user.liga.name;
        }

        if (!$scope.user.equipo.id) { //el usuario no escoge poblacion
            equipo = " ";
        }else{ //el usuario escoge poblacion
            equipo = $scope.user.equipo.name;
        }

// console.log($scope.user);
        //var data = JSON.stringify($scope.user);
        // var data = {"usuario": $scope.user.usuario, "email": $scope.user.email, "nombre": $scope.user.nombre,
        // "apellidos": $scope.user.apellidos, "favorites": $scope.user.favorites, "musicalTastes": musicalTastes, "pais": pais,
        // "provincia": prov,"poblacion": pob, "avatar": $scope.user.avatar, "tipo": tipo};
        var data = JSON.stringify($scope.user);
        // console.log(data);
        var data = {"username": $scope.user.username, "name": $scope.user.name, "email": $scope.user.email,
                    "avatar": $scope.user.userpic, pais: pais, liga: liga, equipo: equipo,
                    "date_birthday": $scope.user.birth_date
                    // ,"password": "puigsolerh"
                  };
        var data_user = JSON.stringify(data);
        console.log(data_user);

        services.put("login", "update_profile", data_user).then(function (response) {
            // console.log(response);
            //console.log(response.user[0].usuario);

            //limpiar el avatar de :80
            console.log(response);
            var avatar = response.user[0].userpic;
            var buscar = avatar.indexOf(":80");
            if(buscar !== -1){
                var avatar = avatar.replace(":80", "");
                response.user[0].avatar = avatar;
            }
            //console.log(response.user[0].avatar);

            if (response.success) {
                localStorage.set(response.user[0]);
                UserService.login();
            } else {
                if (response.datos){
                    //console.log(response.datos);
                    $scope.AlertMessage = true;
                    $timeout(function () {
                        $scope.AlertMessage = false;
                    }, 3000);
                    // $scope.user.user_error = response.datos.usuario;
                    $scope.user.email_error = response.datos.email;
                    $scope.user.nombre_error = response.datos.nombre;
                    // $scope.user.surn_error = response.datos.apellidos;
                    // $scope.user.favorites_error = response.datos.favorites;
                }
            }
        });
    };
});
