eventplanner.controller('menuCtrl', function ($scope, UserService, $rootScope, localStorage) {
    UserService.login();
    $rootScope.bannerV = false;
    $rootScope.bannerText = "";

    $scope.logout = function () {
        UserService.logout();
    };
});
