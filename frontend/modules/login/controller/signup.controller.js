eventplanner.controller('signupCtrl', function ($scope, $route, services, CommonService, $timeout, $location) {
// alert("HOLA");
  $scope.signup = {
      inputUser: "",
      inputEmail: "",
      inputPass: "",
      inputPass2: "",
      inputType: "client"
  };

  $scope.error = function() {
      $scope.signup.user_error = "";
      $scope.signup.email_error = "";
      $scope.signup.pass_error = "";
  };

  $scope.change_signup = function () {
      $scope.signup.user_error = "";
      $scope.signup.email_error = "";
      $scope.signup.pass_error = "";
  };

  $scope.SubmitSignUp = function () {
      var data = {"username": $scope.signup.inputUser, "email": $scope.signup.inputEmail,
          "password": $scope.signup.inputPass, "password2": $scope.signup.inputPass2, "tipo": $scope.signup.inputType};
      var data_users_JSON = JSON.stringify(data);
      console.log(data_users_JSON);
      services.post('login', 'signup_user', data_users_JSON).then(function (response) {
          console.log(response);
          if (response.success) {
              $location.path('/');
              CommonService.toaster("SUCCESS",'top right',5000);
          } else {
              if (response.typeErr === "Name") {
                  $scope.AlertMessage = true;
                  $scope.signup.user_error = response.error;
                  $timeout(function () {
                    $scope.AlertMessage = false;
                    $scope.signup.user_error = "";
                  }, 2000);

              } else if (response.typeErr === "Email") {
                  $scope.AlertMessage = true;
                  $timeout(function () {
                      $scope.AlertMessage = false;
                  }, 5000);
                  $scope.signup.email_error = response.error;

              } else if (response.typeErr === "error") {
                  //console.log(response.error);
                  $scope.AlertMessage = true;
                  $timeout(function () {
                      $scope.AlertMessage = false;
                  }, 5000);
                  $scope.signup.user_error = response.error.usuario;
                  $scope.signup.email_error = response.error.email;
                  $scope.signup.nombre_error = response.error.nombre;
                  $scope.signup.surn_error = response.error.apellidos;
                  $scope.signup.pass_error = response.error.password;
                  $scope.signup.favorites_error = response.error.favorites;
              } else if (response.typeErr === "error_server"){
                  CommonService.toaster("ERROR",'top right',5000);
              }
          }
      });
  };

});
