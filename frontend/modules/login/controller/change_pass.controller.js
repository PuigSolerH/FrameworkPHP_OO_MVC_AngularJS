eventplanner.controller('passwordCtrl', function ($route, $scope, services, $location, CommonService) {
    $scope.token = $route.current.params.token;
    $scope.changepass = {
        inputPassword: ""
    };

    $scope.SubmitChangePass = function () {
        var data = {"password": $scope.changepass.inputPassword, "token": $scope.token};
        var passw = JSON.stringify(data);
        // console.log(passw);

        services.put('login', 'update_pass', passw).then(function (response) {
            console.log(response);
            if (response.success) {
                $location.path('/');
                CommonService.toaster("SUCCESS",'top right',5000);
            } else {
              // console.log(response);
                CommonService.toaster("ERROR",'top right',5000);
            }
        });
    };
});
