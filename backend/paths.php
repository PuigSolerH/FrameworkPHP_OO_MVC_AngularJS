<?php
//PROYECTO
define('PROJECT', '/ennoAngularJS/backend/');

//SITE_ROOT
define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT'] . PROJECT);

//SITE_PATH
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . PROJECT);

//production
define('PRODUCTION', true);

//model
define('MODEL_PATH', SITE_ROOT . 'model/');

//modules
define('MODULES_PATH', SITE_ROOT . 'modules/');

//resources
define('RESOURCES', SITE_ROOT . 'resources/');

//media
define('MEDIA_ROOT', SITE_ROOT . 'media/');
define('MEDIA_PATH', SITE_PATH . 'media/');

//utils
define('UTILS', SITE_ROOT . 'utils/');

//libs
define('LIBS', SITE_ROOT . 'libs/');

//amigables
define('URL_AMIGABLES', TRUE);


//model players
define('MODEL_PLAYERS', SITE_ROOT . 'modules/players/model/model/');
define('DAO_PLAYERS', SITE_ROOT . 'modules/players/model/DAO/');
define('BLL_PLAYERS', SITE_ROOT . 'modules/players/model/BLL/');

//model main
define('MODEL_MAIN', SITE_ROOT . 'modules/main/model/model/');
define('DAO_MAIN', SITE_ROOT . 'modules/main/model/DAO/');
define('BLL_MAIN', SITE_ROOT . 'modules/main/model/BLL/');

//model login
define('UTILS_LOGIN', SITE_ROOT . 'modules/login/utils/');
define('MODEL_LOGIN', SITE_ROOT . 'modules/login/model/model/');

//model like
define('UTILS_LIKE', SITE_ROOT . 'modules/liked/utils/');
define('MODEL_LIKE', SITE_ROOT . 'modules/liked/model/model/');


// //CSS
// define('CSS_PATH', SITE_PATH . 'view/css/');
//
// //JS
// define('JS_PATH', SITE_PATH . 'view/js/');
//
// //IMG
// define('IMG_PATH', SITE_PATH . 'view/img/');
//
// //FONTS
// define('FONTS_PATH', SITE_PATH . 'view/fonts/');
//
// //view
// define('VIEW_PATH_INC', SITE_ROOT . 'view/inc/');
// define('VIEW_PATH_INC_ERROR', SITE_ROOT . 'view/inc/templates_error/');
//
//
//
//
// //main
// define('MAIN_VIEW_PATH', 'modules/main/view/');
// define('FUNCTIONS_MAIN', SITE_ROOT . 'modules/main/utils/');
// define('MODEL_PATH_MAIN', SITE_ROOT . 'modules/main/model/');
// define('DAO_MAIN', SITE_ROOT . 'modules/main/model/DAO/');
// define('BLL_MAIN', SITE_ROOT . 'modules/main/model/BLL/');
// define('MODEL_MAIN', SITE_ROOT . 'modules/main/model/model/');
// define('MAIN_JS_PATH', SITE_PATH . 'modules/main/view/js/');
//
// //model home
// define('HOME_VIEW_PATH', 'modules/home/view/');
// define('FUNCTIONS_HOME', SITE_ROOT . 'modules/home/utils/');
// define('MODEL_PATH_HOME', SITE_ROOT . 'modules/home/model/');
// define('DAO_HOME', SITE_ROOT . 'modules/home/model/DAO/');
// define('BLL_HOME', SITE_ROOT . 'modules/home/model/BLL/');
// define('MODEL_HOME', SITE_ROOT . 'modules/home/model/model/');
// define('HOME_JS_PATH', SITE_PATH . 'modules/home/view/js/');
//
// //model players
// define('PLAYERS_VIEW_PATH', 'modules/players/view/');
// define('FUNCTIONS_PLAYERS', SITE_ROOT . 'modules/players/utils/');
// define('UTILS_PLAYERS', SITE_ROOT . 'modules/players/utils/');
// define('PLAYERS_JS_LIB_PATH', SITE_PATH . 'modules/players/view/lib/');
// define('PLAYERS_JS_PATH', SITE_PATH . 'modules/players/view/js/');
// define('MODEL_PATH_PLAYERS', SITE_ROOT . 'modules/players/model/');
// define('DAO_PLAYERS', SITE_ROOT . 'modules/players/model/DAO/');
// define('BLL_PLAYERS', SITE_ROOT . 'modules/players/model/BLL/');
// define('MODEL_PLAYERS', SITE_ROOT . 'modules/players/model/model/');
//
// //contact
// define('CONTACT_VIEW_PATH', 'modules/contact/view/');
// define('CONTACT_CSS_PATH', SITE_PATH . 'modules/contact/view/css/');
// define('CONTACT_LIB_PATH', SITE_PATH . 'modules/contact/view/lib/');
// define('CONTACT_JS_PATH', SITE_PATH . 'modules/contact/view/js/');
// define('CONTACT_IMG_PATH', SITE_PATH . 'modules/contact/view/img');
//
// //login
// define('LOGIN_VIEW_PATH', 'modules/login/view/');
// define('LOGIN_JS_PATH', SITE_PATH . '/modules/login/view/js/');
// define('LOGIN_CSS_PATH', SITE_PATH . '/modules/login/view/css/');
// define('UTILS_LOGIN', SITE_ROOT . '/modules/login/utils/');
// define('MODEL_LOGIN', SITE_ROOT . '/modules/login/model/model/');
