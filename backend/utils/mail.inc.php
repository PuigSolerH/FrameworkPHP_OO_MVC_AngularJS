<?php
    function enviar_email($arr) {
        $html = '';
        $subject = '';
        $body = '';
        $ruta = '';
        $return = '';

        switch ($arr['type']) {
            case 'alta':
                $subject = 'Tu Alta en Enno';
                $ruta = "<a href='" . "http://localhost/ennoAngularJS/#/login/verify/" . $arr['token'] . "'>aqu&iacute;</a>";
                $body = 'Gracias por unirte a nuestra aplicaci&oacute;n<br> Para finalizar el registro, pulsa ' . $ruta;
                break;

            case 'modificacion':
                $subject = 'Tu Nuevo Password en Enno<br>';
                $ruta = '<a href="' . "http://localhost/ennoAngularJS/#/login/password/" . $arr['token'] . '">aqu&iacute;</a>';
                $body = 'Para recordar tu password pulsa ' . $ruta;
                break;

            case 'contact':
                $subject = 'Tu Petición a Enno ha sido enviada<br>';
                // $ruta = '<a href=' . 'http://localhost/ennoAngularJS/'. '>aqu&iacute;</a>';
                $ruta = '<a href="http://'.$_SERVER['HTTP_HOST'].'/ennoAngularJS/#/"' . '>aqu&iacute;</a>';
                $body = 'Para visitar nuestra web, pulsa ' . $ruta;
                break;

            case 'admin':
                $subject = $arr['inputSubject'];
                $body = 'inputName: ' . $arr['inputName']. '<br>' .
                'inputEmail: ' . $arr['inputEmail']. '<br>' .
                'inputSubject: ' . $arr['inputSubject']. '<br>' .
                'inputMessage: ' . $arr['inputMessage'];
                break;
        }

        $html .= "<html>";
        $html .= "<head>";
        $html .= "<meta charset='utf-8' />
        <style>
                * {
                    margin: 0;
                    padding: 0;
                    text-align: center;
                  }

                body {
                    margin: 0 auto;
                    width: 600px;
                    height: 300px;
                }

                header {
                    padding: 20px;
                    background-color: blue;
                    color: white;
                    padding-left: 20px;
                    font-size: 25px;
                }

                section {
                    padding-top: 50px;
                    padding-left: 50px;
                    margin-top: 3px;
                    margin-bottom: 3px;
                    height: 100px;
                    background-color: ghostwhite;
                  }

                 footer {
                    padding: 5px;
                    padding-left: 20px;
                    background-color: blue;
                    color: white;
                  }
            </style>";
        $html .= "</head>";
            $html .= "<body>";
            $html .= "<header>";
                $html .= "<p>" . $subject . "</p>";
            $html .= "</header>";
            $html .= "<section>";
                $html .= $body;
            $html .= "</section>";
            $html .= "<footer>";
                $html .= "<p>Enviado por Hiber Puig</p>";
            $html .= "</footer>";
        $html .= "</body>";
        $html .= "</html>";

        set_error_handler('ErrorHandler');
        try{
            if ($arr['type'] === 'admin')
                $address = 'hibe98@gmail.com';
            else
                $address = $arr['inputEmail'];
            $result = send_mailgun('hiber98@gmail.com', $address, $subject, $html);
        } catch (Exception $e) {
			$return = 0;
		}
		restore_error_handler();
        return $result;
    }

    function send_mailgun($from, $to, $subject, $html){
          $config = array();
          $config['api_key'] = "key-24df9e85013e1ef82505e10ec1267159"; //API Key
          $config['api_url'] = "https://api.mailgun.net/v3/sandbox66049a3ee75446eab860c5ceef0bf915.mailgun.org/messages"; //API Base URL

          $message = array();
          $message['from'] = $from;
          $message['to'] = $to;
          $message['h:Reply-To'] = "hiber98@gmail.com";
          $message['subject'] = $subject;
          $message['html'] = $html;
          // $message['html'] = 'Hello ' . $email . ',</br></br> This is a test';

          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $config['api_url']);
          curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
          curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
          $result = curl_exec($ch);
          curl_close($ch);
          return $result;
    }
