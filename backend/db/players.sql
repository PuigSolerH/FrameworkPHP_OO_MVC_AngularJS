-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 10-06-2018 a las 15:45:53
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `3eva`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `player` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `user` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comments`
--

INSERT INTO `comments` (`id`, `player`, `user`, `comment`) VALUES
(1, '1', 'puigsolerh', 'Aquest jugador es molt bo'),
(2, '1', 'hiber98', 'Rodrigol!!'),
(3, '1', 'puigsolerh', 'HOLA'),
(4, '2', 'hiber98', 'Aquest es un comentari de prova'),
(5, '1', 'hiber98', 'Aquest es un comentari de prova'),
(9, '1', 'hiber98', 'Anem a veure si funciona.'),
(8, '1', 'hiber98', 'Juga al ValenciaCF'),
(10, '1', 'hiber98', 'dsfgdsfgsdfgfg'),
(11, '2', 'puigsolerh', 'Es un jugador de Torrent');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `name`) VALUES
(1, 'Spain'),
(2, 'England'),
(3, 'Italy'),
(4, 'Germany'),
(5, 'France');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `leagues`
--

DROP TABLE IF EXISTS `leagues`;
CREATE TABLE IF NOT EXISTS `leagues` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `id_country` int(200) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_country` (`id_country`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `leagues`
--

INSERT INTO `leagues` (`id`, `id_country`, `name`) VALUES
(1, 1, 'Liga Santander'),
(2, 1, 'Liga 123'),
(3, 2, 'Premier League'),
(4, 2, 'Championship'),
(5, 3, 'Serie A'),
(6, 3, 'Serie B'),
(7, 4, 'Bundesliga'),
(8, 4, '2 Liga'),
(9, 5, 'Ligue 1'),
(10, 5, 'Ligue 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `liked`
--

DROP TABLE IF EXISTS `liked`;
CREATE TABLE IF NOT EXISTS `liked` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `player` int(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `liked`
--

INSERT INTO `liked` (`id`, `username`, `player`) VALUES
(1, 'puigsolerh', 1),
(2, 'puigsolerh', 2),
(3, 'puigsolerh', 73),
(4, 'puigsolerh', 14),
(9, 'puigsolerh', 3),
(6, 'puigsolerh', 80),
(7, 'puigsolerh', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `players`
--

DROP TABLE IF EXISTS `players`;
CREATE TABLE IF NOT EXISTS `players` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `surname` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `position` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `foot` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `country` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `league` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `team` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `signin` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `expiration` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `appearances` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `img` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `lat` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `lon` varchar(80) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `players`
--

INSERT INTO `players` (`id`, `name`, `surname`, `position`, `foot`, `country`, `league`, `team`, `signin`, `expiration`, `appearances`, `description`, `img`, `lat`, `lon`) VALUES
(1, 'Rodrigo', 'Moreno', 'striker', 'a', '3', 'a', 'a', 'a', 'a', '1', 'a', '/enno/media/rodrigol2.png', '38.810126', '-0.6042581'),
(2, 'Paco', 'Alcacer', 'striker', 'b', '1', 'b', 'b', 'b', 'b', '2', 'b', '/enno/media/imagen2.jpg', '38.88991', '-0.337066'),
(3, 'David', 'deGea', 'goalkeeper', 'c', '4', 'c', 'c', 'c', 'c', '2', 'c', '/enno/media/imagen3.jpg', '38.932296', '-0.25689'),
(4, 'Goncalo', 'Guedes', 'midfielder', 'd', '4', 'd', 'd', 'd', 'd', '2', 'd', '/enno/media/imagen1.jpg', '38.759573', '-0.611643'),
(5, 'Dani', 'Parejo', 'midfielder', 'foot', '2', 'league', 'team', 'signin', 'expiration', '2', 'description', '/enno/media/imagen3.jpg', '39.072603', '-0.258232'),
(6, 'Geoffrey', 'Kondogbia', 'midfielder', 'fsdt', '5', 'league', 'team', 'signin', 'expiration', '9', 'description', '/enno/media/rodrigol2.png', '38.992972', '-0.526839'),
(7, 'Carlos', 'Soler', 'midfielder', 'saoot', '3', 'league', 'team', 'signin', 'expiration', '435', 'description', '/enno/media/imagen2.jpg', '39.129241', '-0.7859'),
(8, 'Simone', 'Zaza', 'striker', 'fsdt', '2', 'league', 'team', 'signin', 'expiration', '1234', 'description', '/enno/media/imagen1.jpg', '39.152252', '-0.436683'),
(9, 'Sergio', 'Aguero', 'striker', 'asd', '1', 'league', 'team', 'signin', 'expiration', '12', 'description', '/enno/media/imagen3.jpg', '39.46891', '-0.375738'),
(10, 'Edinson', 'Cavani', 'striker', 'cvxb', '5', 'league', 'team', 'signin', 'expiration', '65', 'description', '/enno/media/rodrigol2.png', '39.161006', '-0.254793'),
(11, 'Juan', 'Bernat', 'defender', 'zxc', '3', 'league', 'team', 'signin', 'expiration', '78', 'description', '/enno/media/rodrigol2.png', '38.840602', '0.105593'),
(73, 'Harry', 'Kane', 'defender', 'left:right:', '1', '1', '1', '01/04/2018', '30/04/2018', '2', 'f', '/enno/media/default-avatar.png', NULL, NULL),
(84, 'Costas', 'Manolas', 'goalkeeper', 'left:', '5', '9', '89', '01/05/2018', '31/05/2018', '213213', 'sadfsdaf', '/enno/media/prova3.jpg', NULL, NULL),
(85, 'Pepe', 'Reina', 'striker', 'left:', '2', '4', '38', '01/05/2018', '31/05/2018', '1234', 'sdfsadf', '/enno/media/imagen2.jpg', NULL, NULL),
(86, 'Hiber', 'Puig', 'defender', 'left:right:', '2', '3', '22', '01/05/2018', '31/05/2018', '12312', 'fdsdgbdgn', '/enno/media/prova1.jpg', NULL, NULL),
(80, 'Cristian', 'Portugues', 'goalkeeper', 'left:right:', '1', '1', '7', '01/04/2018', '30/04/2018', '12432', 'sfdadfasd', '/enno/media/default-avatar.png', NULL, NULL),
(81, 'Raul', 'deTomas', 'goalkeeper', 'left:right:', '4', '7', '67', '01/04/2018', '30/04/2018', '2432', 'sdgdsfgsdfg', '/enno/media/default-avatar.png', NULL, NULL),
(82, 'hibernon', 'puig', 'striker', 'left:', '2', '3', '21', '01/05/2018', '31/05/2018', '123', 'sdfasfsfasfasdfs', '/enno/media/prova3.jpg', NULL, NULL),
(83, 'hibernonsdf', 'sdfasd', 'goalkeeper', 'left:', '4', '7', '70', '28/05/2018', '31/05/2018', '213', '213213', '/enno/media/imagen1.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teams`
--

DROP TABLE IF EXISTS `teams`;
CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `id_league` int(50) DEFAULT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_league` (`id_league`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `teams`
--

INSERT INTO `teams` (`id`, `id_league`, `name`) VALUES
(1, 1, 'FC Barcelona'),
(2, 1, 'Atl Madrid'),
(3, 1, 'Real Madrid'),
(4, 1, 'Valencia CF'),
(5, 1, 'Sevilla FC'),
(6, 1, 'Villarreal CF'),
(7, 1, 'Girona FC'),
(8, 1, 'Real Betis'),
(9, 1, 'SD Eibar'),
(10, 1, 'RC Celta de Vigo'),
(11, 2, 'Huesca'),
(12, 2, 'Rayo Vallecano'),
(13, 2, 'Cadiz'),
(14, 2, 'Real Sporting'),
(15, 2, 'Granada'),
(16, 2, 'Osasuna'),
(17, 2, 'Real Oviedo'),
(18, 2, 'Real Zaragoza'),
(19, 2, 'Numancia'),
(20, 2, 'Real Valladolid'),
(21, 3, 'Manchester City'),
(22, 3, 'Manchester United'),
(23, 3, 'Tottenham'),
(24, 3, 'Liverpool'),
(25, 3, 'Chelsea'),
(26, 3, 'Arsenal'),
(27, 3, 'Burnley'),
(28, 3, 'Leicester'),
(29, 3, 'Everton'),
(30, 3, 'Watford'),
(31, 4, 'Wolverhampton'),
(32, 4, 'Cardiff City'),
(33, 4, 'Aston Villa'),
(34, 4, 'Fullham'),
(35, 4, 'Derby Country'),
(36, 4, 'Middlesbrough'),
(37, 4, 'Sheffield United'),
(38, 4, 'Bristol City'),
(39, 4, 'Preston North End'),
(40, 4, 'Millwall'),
(41, 5, 'Juventus'),
(42, 5, 'Napoli'),
(43, 5, 'Roma'),
(44, 5, 'Lazio'),
(45, 5, 'Inter'),
(46, 5, 'Milan'),
(47, 5, 'Sampdoria'),
(48, 5, 'Atalanta'),
(49, 5, 'Fiorentina'),
(50, 5, 'Torino'),
(51, 6, 'Empoli'),
(52, 6, 'Frosinone'),
(53, 6, 'Palermo'),
(54, 6, 'Cittadella'),
(55, 6, 'Venezia'),
(56, 6, 'AS Bari'),
(57, 6, 'Parma'),
(58, 6, 'Perugia'),
(59, 6, 'Spezia'),
(60, 6, 'Carpi'),
(61, 7, 'Bayern Munchen'),
(62, 7, 'Schalke 04'),
(63, 7, 'Borussia Dormund'),
(64, 7, 'Bayern Leverkusen'),
(65, 7, 'Eintracht Frankfurt'),
(66, 7, 'RB Leipzig'),
(67, 7, 'Hoffenheim'),
(68, 7, 'FC Ausburg'),
(69, 7, 'Borussia Monchengladbach'),
(70, 7, 'Stuttgart'),
(71, 8, 'Fortuna Dusseldorf'),
(72, 8, 'Nurnberg'),
(73, 8, 'Holstein Kiel'),
(74, 8, 'Jahn Regensburg'),
(75, 8, 'Arminia Bielefeld'),
(76, 8, 'MSV Diusburg'),
(77, 8, 'Sandhausen'),
(78, 8, '1 FC Union Berlin'),
(79, 8, 'Dynamo Dresden'),
(80, 8, 'FC St Pauli'),
(81, 9, 'PSG'),
(82, 9, 'Monaco'),
(83, 9, 'Olympique Marseille'),
(84, 9, 'Olympique Lyon'),
(85, 9, 'Nantes'),
(86, 9, 'Rennes'),
(87, 9, 'Nice'),
(88, 9, 'Montpellier'),
(89, 9, 'Girondins Bourdeaux'),
(90, 9, 'Dijon FCO'),
(91, 10, 'Stade de Reims'),
(92, 10, 'Nîmes'),
(93, 10, 'Ajaccio'),
(94, 10, 'Lorient'),
(95, 10, 'Paris FC'),
(96, 10, 'Clermont'),
(97, 10, 'Chateauroux'),
(98, 10, 'Le Havre'),
(99, 10, 'Stade Brestois'),
(100, 10, 'Sochaux');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `birth_date` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `country` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `league` varchar(200) CHARACTER SET utf16 COLLATE utf16_spanish_ci DEFAULT NULL,
  `team` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `userpic` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `activado` int(255) DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `token_get` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`username`, `name`, `birth_date`, `email`, `password`, `country`, `league`, `team`, `userpic`, `tipo`, `activado`, `token`, `token_get`) VALUES
('PF6NCe5KVFOCdHl8w70mdg8qLS92', 'Hiber Puig Soler', NULL, 'hiber98@hotmail.com', '', NULL, NULL, NULL, 'https://graph.facebook.com/1999951220037770/picture', 'client', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1MjgyMDY0MjIsIm5iZiI6MTUyOTIwNjQyMSwibmFtZSI6IkhpYmVyIFB1aWcgU29sZXIiLCJlbWFpbCI6ImhpYmVyOThAaG90bWFpbC5jb20ifQ.TinkrET84bxAvLaNqmtCamWvk6Mtz6ttYIKZln1L4Ithu5A5496fyAJTBRS4HMeG_-diW0RETF1od2alVv49pw', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Mjg1NjMzMTksIm5iZiI6MTUyOTU2MzMxOCwibmFtZSI6IkhpYmVyIFB1aWcgU29sZXIiLCJlbWFpbCI6ImhpYmVyOThAaG90bWFpbC5jb20ifQ.lX5XvyLTnBMo8WYcVCG8SplGr34Zf-a4ic2LGv54WKTGRgJVMSinfGIIy3WVpbLqJWPO8L6x7S0-OT9iJR98pQ'),
('9tWSqQ1k0iYK6Ae8wCbcK9UaZwE2', 'Hiber Puig Soler', NULL, '', '', NULL, NULL, NULL, 'https://pbs.twimg.com/profile_images/779395476831498240/9MoVz625_normal.jpg', 'client', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1MjgyMDY0NDgsIm5iZiI6MTUyOTIwNjQ0NywibmFtZSI6IkhpYmVyIFB1aWcgU29sZXIiLCJlbWFpbCI6bnVsbH0.nSNfl9paEODqUkeXUugRTlxQA9rKG-UTrPe9tDAaO6sAz_sHOyaTPjKh-dded7GwXN5-OGxAIELWPhWH8_e7Ow', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1MjgyMTI2MzcsIm5iZiI6MTUyOTIxMjYzNiwibmFtZSI6IkhpYmVyIFB1aWcgU29sZXIiLCJlbWFpbCI6bnVsbH0.RVIm2irTM02_0d_6io-p2G4q8MVpf3_BSf9ReFvcOFaXqYfoxUkOLzL1aBtKH-SAiXxcB8EDnVY8CvtYuxWAoQ'),
('118182602349992197293', 'Hiber Puig Soler', NULL, 'hiber98@gmail.com', '', NULL, NULL, NULL, 'https://lh3.googleusercontent.com/-t2cS9CYczBo/AAAAAAAAAAI/AAAAAAAAAao/uw7Si_rffes/photo.jpg', 'client', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1MjgyMDY0NjIsIm5iZiI6MTUyOTIwNjQ2MSwibmFtZSI6IkhpYmVyIFB1aWcgU29sZXIiLCJlbWFpbCI6ImhpYmVyOThAZ21haWwuY29tIn0.wi7s2RtvOaG2_T1C_apvdLmgPpWfezuBRDOcnAIxINrRK7nwyJE9Xk-Q24h1hnzVPERDSHRfaBwy-JUso4U7Fg', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1MjgyMTI2NTYsIm5iZiI6MTUyOTIxMjY1NSwibmFtZSI6IkhpYmVyIFB1aWcgU29sZXIiLCJlbWFpbCI6ImhpYmVyOThAZ21haWwuY29tIn0.TBtnSPL4wL-ANS2ba2kDr79GSH_oNyP1k6utAFSyHApxVZw2UEoYC8TsMckKJyHp7f-ZrpsiGiRDePqqx_L_SQ'),
('puigsolerh', 'Hiber Puig', '05/06/2018', 'hiber98.test@gmail.com', '$2y$10$27.xWpT4bIF/eNh71OR1HOBgAC/4rCQaotTMjiaugkUaZfCsT368e', 'Spain', 'Liga 123', 'Real Sporting', 'http://localhost/ennoAngularJS/backend/media/user1.png', 'client', 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1MjgyMTI4MTIsIm5iZiI6MTUyOTIxMjgxMSwibmFtZSI6bnVsbCwiZW1haWwiOm51bGx9.wFIGJmFesOMSvyZtDUR7mWAYPlsS8jepcYM90-k4YgoYqoM3YCGeSLYuVbQCygjEyHnyA0u1gibutBKY_Z8IWw', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Mjg2NDI5NDAsIm5iZiI6MTUyOTY0MjkzOSwibmFtZSI6bnVsbCwiZW1haWwiOm51bGx9.recjGzo21Pd5IgBQWnPJEy3y2dxsRv9A5j4mskQt9l8CaiOtuiZyZMfc0tJM9raSfvkbDHsl_az7IrfTHokrCQ'),
('hiber98', '', NULL, 'hiberprova1@gmail.com', '$2y$10$PnQ9qLqgd.kk5jjVQnt1KObf/4RUBHQ3Wi2uu4lHV27FCm91pgi0y', NULL, NULL, NULL, 'https://www.gravatar.com/avatar/38e6233c98fda2de76aeeb52df45d1da38e6233c98fda2de76aeeb52df45d1da?s=400&d=identicon&r=g', 'client', 1, 'Vera8f889086f2c8d13e814c96811db347c', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1Mjg2MTI3ODUsIm5iZiI6MTUyOTYxMjc4NCwibmFtZSI6bnVsbCwiZW1haWwiOm51bGx9.sWHVyJ4WKYAndjSj8fN2xngYmFX1JfQcT1ZO7OVc_EzqPwdSLHlN4M4w1IsaYfZat8YUutYCAITnRce9kXbWVw');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
