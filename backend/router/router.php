<?php
require_once("paths.php");
require 'autoload.php';

include(UTILS . "filters.inc.php");
include(UTILS . "utils.inc.php");
include(UTILS . "response_code.inc.php");
include(UTILS . "common.inc.php");
include(UTILS . "mail.inc.php");

// if (PRODUCTION) { //estamos en producción
//     ini_set('display_errors', '1');
//     ini_set('error_reporting', E_ERROR | E_WARNING); //error_reporting(E_ALL) ;
//     //error_reporting(E_ALL) ; | E_NOTICE --> commit E_NOTICE to use timeout userdao_country
// } else {
//     ini_set('display_errors', '0');
//     ini_set('error_reporting', '0'); //error_reporting(0);
// }

//ob_start();
session_start();
$_SESSION['module'] = "";
$_POST = json_decode(file_get_contents('php://input'), true);

function handlerRouter() {
    if (!empty($_GET['module'])) {
        $URI_module = $_GET['module'];
    } else {
        $URI_module = 'main';
    }

    if (!empty($_GET['function'])) {
        $URI_function = $_GET['function'];
    } else {
        $URI_function = 'begin';
    }
    handlerModule($URI_module, $URI_function);
}

function handlerModule($URI_module, $URI_function) {
    $modules = simplexml_load_file('resources/modules.xml');
    $exist = false;

    foreach ($modules->module as $module) {
        if (($URI_module === (String) $module->uri)) {
            $exist = true;

            $path = MODULES_PATH . $URI_module . "/controller/controller_" . $URI_module . ".class.php";
            // echo json_encode($path);
            // die();
            if (file_exists($path)) {
                require_once($path);
                $controllerClass = "controller_" . $URI_module;
                // echo json_encode($controllerClass);
                // die();
                $obj = new $controllerClass;
                // echo json_encode($obj);
                // die();
            } else {
              // echo '<script language="javascript">alert("'.$_GET['module'].'");</script>';
              // echo '<script language="javascript">alert("'.$_GET['obj'].'");</script>';
              // echo '<script language="javascript">alert("juas1");</script>';
                echo json_encode($obj['error']=404);
            }
            handlerfunction(((String) $module->name), $obj, $URI_function);
            break;
        }
    }
    if (!$exist) {
        // echo '<script language="javascript">alert("'.$_GET['module'].'");</script>';
        //   echo '<script language="javascript">alert("'.$_GET['function'].'");</script>';
        //   echo '<script language="javascript">alert("juas2");</script>';
        echo json_encode($obj['error']=404);
    }
}

function handlerFunction($module, $obj, $URI_function) {
  // echo json_encode($module);
  // die();
    $functions = simplexml_load_file(MODULES_PATH . $module . "/resources/functions.xml");
      // echo json_encode($functions);
      // die();
    $exist = false;

    foreach ($functions->function as $function) {
        if (($URI_function === (String) $function->uri)) {
            $exist = true;
            $event = (String) $function->name;
            break;
        }
    }
    if (!$exist) {
      echo json_encode($obj['error']="404".$module.$URI_function);
      // echo '<script language="javascript">alert("'.$_GET['function'].'");</script>';
      // echo '<script language="javascript">alert("juas3");</script>';
    } else {
        //$obj->$event();
        call_user_func(array($obj, $event));
    }
}

handlerRouter();
