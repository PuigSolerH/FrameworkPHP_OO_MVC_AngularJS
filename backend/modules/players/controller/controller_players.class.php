<?php
class controller_players {

  public function __construct() {
      $_SESSION['module'] = "players";
  }

    /**
     *  We take all offers from BD and we return them
     *
     * @return mixed[] Returns an array['success']=boolean and if it is true we return the array array['ofertas']=array with all offers.
     */
    function all_players() {
        set_error_handler('ErrorHandler');
        try {
            $arrValue = loadModel(MODEL_PLAYERS, "players_model", "select_all");
        } catch (Exception $e) {
            $arrValue = false;
        }
        restore_error_handler();

        if ($arrValue) {
            $arrArguments['players'] = $arrValue;
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {

            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }
    }

    function player_details() {
        set_error_handler('ErrorHandler');
        try {
            $arrValue = loadModel(MODEL_PLAYERS, "players_model", "players_details",$_GET['param']);
        } catch (Exception $e) {
            $arrValue = false;
        }
        restore_error_handler();

        if ($arrValue) {
            $arrArguments['players'] = $arrValue[0];
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {

            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }
    }

    function players_filtered() {
        set_error_handler('ErrorHandler');
        try {
            $arrValue = loadModel(MODEL_PLAYERS, "players_model", "players_type", $_GET['param']);
        } catch (Exception $e) {
            $arrValue = false;
        }
        restore_error_handler();

        if ($arrValue) {
            $arrArguments['players'] = $arrValue;
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {

            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }
    }

    function getfavorites() {
      // echo json_encode($_POST['usuario']);
      // die();
        set_error_handler('ErrorHandler');
        try {
            $arrValue = loadModel(MODEL_PLAYERS, "players_model", "get_favorites",$_POST['usuario']);
            // echo json_encode($arrValue);
            // die();
        } catch (Exception $e) {
            $arrValue = false;
        }
        restore_error_handler();
        echo json_encode($arrValue);
    }

    function addfavorites() {
      // echo json_encode($_POST);
      // die();
        set_error_handler('ErrorHandler');
        $arrArgument = array(
            'username' => $_POST['username'],
            'player' => $_POST['favorite']
        );
        // echo json_encode($arrArgument);
        // die();

        try {
            $arrValue = loadModel(MODEL_PLAYERS, "players_model", "count_favorites",$arrArgument);
            // echo json_encode($arrValue);
            // die();
        } catch (Exception $e) {
            $arrValue = false;
        }
        restore_error_handler();

        if ($arrValue[0]["COUNT(*)"] == 0) {
          set_error_handler('ErrorHandler');
          $player = loadModel(MODEL_PLAYERS, "players_model", "add_favorites",$arrArgument);

          if($player) {
            restore_error_handler();
            $jsondata["success"] = true;
            // $jsondata['player'] = $player;
            echo json_encode($jsondata);
            exit();
          } else {
            $jsondata["error"] = true;
            $jsondata['typeErr'] = "error_server";
            echo json_encode($jsondata);
            exit;
          }
        } else {
            $jsondata["player"] = true;
            $jsondata['typeErr'] = "player has already been added by you";
            echo json_encode($jsondata);
            exit;
        }
    }

    function removefavorites() {
      // echo json_encode($_POST);
      // die();
        set_error_handler('ErrorHandler');
        $arrArgument = array(
            'username' => $_POST['username'],
            'player' => $_POST['favorite']
        );
        // echo json_encode($arrArgument);
        // die();

        try {
            $arrValue = loadModel(MODEL_PLAYERS, "players_model", "remove_favorites",$arrArgument);
            // echo json_encode($player);
            // die();
        } catch (Exception $e) {
            $arrValue = false;
        }

          if($arrValue) {
            restore_error_handler();
            $jsondata["success"] = true;
            // $jsondata['player'] = $player;
            echo json_encode($jsondata);
            exit();
          } else {
            $jsondata["error"] = true;
            $jsondata['typeErr'] = "error_server";
            echo json_encode($jsondata);
            exit;
          }
    }

    function player_comments() {
        set_error_handler('ErrorHandler');
        try {
            $arrValue = loadModel(MODEL_PLAYERS, "players_model", "players_comments",$_GET['param']);
        } catch (Exception $e) {
            $arrValue = false;
        }
        restore_error_handler();

        if ($arrValue) {
            $arrArguments['players'] = $arrValue;
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {

            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }
    }

    function create_comment() {
      // echo json_encode($_POST);
      // die();
        set_error_handler('ErrorHandler');
        $arrArgument = array(
            'user' => $_POST['user'],
            'player' => $_POST['player'],
            'comment' => $_POST['comment']
        );
        // echo json_encode($arrArgument);
        // die();

        try {
            $arrValue = loadModel(MODEL_PLAYERS, "players_model", "create_comments",$arrArgument);
            // echo json_encode($player);
            // die();
        } catch (Exception $e) {
            $arrValue = false;
        }

          if($arrValue) {
            restore_error_handler();
            $jsondata["success"] = true;
            // $jsondata['player'] = $player;
            echo json_encode($jsondata);
            exit();
          } else {
            $jsondata["error"] = true;
            $jsondata['typeErr'] = "error_server";
            echo json_encode($jsondata);
            exit;
          }
    }

}
