<?php
class players_bll {
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = players_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function select_all_BLL($arrArgument) {
        return $this->dao->select_all_DAO($this->db, $arrArgument);
    }

    public function players_details_BLL($arrArgument) {
        return $this->dao->players_details_DAO($this->db, $arrArgument);
    }

    public function players_comments_BLL($arrArgument) {
        return $this->dao->players_comments_DAO($this->db, $arrArgument);
    }

    public function players_type_BLL($arrArgument) {
        return $this->dao->players_type_DAO($this->db, $arrArgument);
    }

    public function get_favorites_BLL($arrArgument) {
        return $this->dao->get_favorites_DAO($this->db, $arrArgument);
    }

    public function add_favorites_BLL($arrArgument) {
        return $this->dao->add_favorites_DAO($this->db, $arrArgument);
    }

    public function count_favorites_BLL($arrArgument) {
        return $this->dao->count_favorites_DAO($this->db, $arrArgument);
    }

    public function remove_favorites_BLL($arrArgument) {
        return $this->dao->remove_favorites_DAO($this->db, $arrArgument);
    }

    public function create_comments_BLL($arrArgument) {
        return $this->dao->create_comments_DAO($this->db, $arrArgument);
    }

}
