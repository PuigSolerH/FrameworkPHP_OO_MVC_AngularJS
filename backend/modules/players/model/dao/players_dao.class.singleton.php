<?php
class players_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function select_all_DAO($db) {
        $sql = "SELECT * FROM players";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function players_details_DAO($db,$id) {
        $sql = "SELECT * FROM players WHERE id=".$id;
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
      }

    public function players_type_DAO($db,$type) {
        $sql = "SELECT * FROM players WHERE country ='$type'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function get_favorites_DAO($db, $arrArgument) {
      // echo json_encode($arrArgument);
      // die();
      // $id = $arrArgument['usuario'];
      // echo json_encode($id);
      // die();
      $sql = "SELECT * FROM `liked` INNER JOIN players ON players.id = liked.player WHERE username LIKE '$arrArgument'";
      $stmt = $db->ejecutar($sql);
      return $db->listar($stmt);
    }

    public function add_favorites_DAO($db, $arrArgument) {
        $username = $arrArgument['username'];
        $player = $arrArgument['player'];
        //echo json_encode($usuario);
        $sql = "INSERT INTO liked (username, player) VALUES ('$username','$player')";
        // echo json_encode($sql);

        return $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function count_favorites_DAO($db, $arrArgument) {
        $username = $arrArgument['username'];
        $player = $arrArgument['player'];
        // echo json_encode($player);
        // die();
        $sql = "SELECT COUNT(*) FROM liked WHERE username LIKE '$username' AND player LIKE '$player'";
        // echo json_encode($sql);
        // die();
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function remove_favorites_DAO($db, $arrArgument) {
      // echo json_encode($arrArgument);
      // die();
        $username = $arrArgument['username'];
        $player = $arrArgument['player'];
        //echo json_encode($usuario);
        $sql = "DELETE FROM liked WHERE username LIKE '$username' AND player LIKE '$player'";
        // echo json_encode($sql);
        // die();

        return $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function players_comments_DAO($db,$id) {
        $sql = "SELECT * FROM comments WHERE player LIKE '$id'";
        // $sql = "SELECT * FROM comments";
        // echo json_encode($sql);
        // die();
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function create_comments_DAO($db,$arrArgument) {
      $player = $arrArgument['player'];
      $user = $arrArgument['user'];
      $comment = $arrArgument['comment'];

      $sql = "INSERT INTO comments (player, user, comment) VALUES ('$player', '$user', '$comment')";

      return $db->ejecutar($sql);
      return $db->listar($stmt);
    }

}
