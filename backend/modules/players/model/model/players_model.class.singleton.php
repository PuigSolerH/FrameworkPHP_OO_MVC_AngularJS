<?php
class players_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = players_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function select_all($arrArgument) {
        return $this->bll->select_all_BLL($arrArgument);
    }

    public function players_details($arrArgument) {
        return $this->bll->players_details_BLL($arrArgument);
    }

    public function players_comments($arrArgument) {
        return $this->bll->players_comments_BLL($arrArgument);
    }

    public function players_type($arrArgument) {
        return $this->bll->players_type_BLL($arrArgument);
    }

    public function get_favorites($arrArgument) {
        return $this->bll->get_favorites_BLL($arrArgument);
    }

    public function add_favorites($arrArgument) {
        return $this->bll->add_favorites_BLL($arrArgument);
    }

    public function count_favorites($arrArgument) {
        return $this->bll->count_favorites_BLL($arrArgument);
    }

    public function remove_favorites($arrArgument) {
        return $this->bll->remove_favorites_BLL($arrArgument);
    }

    public function create_comments($arrArgument) {
        return $this->bll->create_comments_BLL($arrArgument);
    }

}
