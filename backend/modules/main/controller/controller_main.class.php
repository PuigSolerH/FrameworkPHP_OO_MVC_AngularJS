<?php
class controller_main {

  public function __construct() {
      $_SESSION['module'] = "main";
  }

    /**
     *  We take all offers from BD and we return them
     *
     * @return mixed[] Returns an array['success']=boolean and if it is true we return the array array['ofertas']=array with all offers.
     */
    function list_categories() {
        set_error_handler('ErrorHandler');
        try {
            $arrValue = loadModel(MODEL_MAIN, "main_model", "select_categories");
        } catch (Exception $e) {
            $arrValue = false;
        }
        restore_error_handler();

        if ($arrValue) {
            $arrArguments['main'] = $arrValue;
            $arrArguments['success'] = true;
            echo json_encode($arrArguments);
        } else {

            $arrArguments['success'] = false;
            $arrArguments['error'] = 503;
            echo json_encode($arrArguments);
        }
    }
}
