<?php
class main_bll {
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = main_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function select_categories_BLL($arrArgument) {
        return $this->dao->select_categories_DAO($this->db, $arrArgument);
    }
}
