<?php
class main_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function select_categories_DAO($db) {
        $sql = "SELECT * FROM countries";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }
}
