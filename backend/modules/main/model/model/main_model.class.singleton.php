<?php
class main_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = main_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function select_categories($arrArgument) {
        return $this->bll->select_categories_BLL($arrArgument);
    }
}
