<?php
    class controller_contact {

        public function __construct() {
            $_SESSION['module'] = "contact";
        }

        public function process_contact() {
            if($_POST['token'] === "contact_form"){

                //////////////// Envio del correo al usuario
                $arrArgument = array(
									'type' => 'contact',
									'token' => '',
									'inputName' => $_POST['inputName'],
									'inputEmail' => $_POST['inputEmail'],
									'inputSubject' => $_POST['inputSubject'],
									'inputMessage' => $_POST['inputMessage']
								);
      				set_error_handler('ErrorHandler');
      				try{
                  enviar_email($arrArgument);
                  echo "|true|SUCCESS";
      				} catch (Exception $e) {
      					echo "|false|error";
      				}
      				restore_error_handler();


                //////////////// Envio del correo al admin de la web
                $arrArgument = array(
									'type' => 'admin',
									'token' => '',
									'inputName' => $_POST['inputName'],
									'inputEmail' => $_POST['inputEmail'],
									'inputSubject' => $_POST['inputSubject'],
									'inputMessage' => $_POST['inputMessage']
								);
                set_error_handler('ErrorHandler');
      				try{
                          // sleep(5);
                  enviar_email($arrArgument);
                  // echo "|true|SUCCESS";
                  exit();
      				} catch (Exception $e) {
      					echo "|false|ERROR";
      				}
      				restore_error_handler();

            }else{
                echo "|false|ERROR";
            }
        }
    }
