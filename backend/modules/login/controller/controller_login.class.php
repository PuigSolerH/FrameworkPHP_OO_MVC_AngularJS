<?php
use \Firebase\JWT\JWT;

class controller_login {

  function __construct() {
      require_once(UTILS_LOGIN . "functions.inc.php");
      include(UTILS . 'upload.inc.php');
      // include(LIBS . 'password_compat-master/lib/password.php');
      // include(UTILS . 'upload.php');
      include_once(LIBS . 'php-jwt-master/src/JWT.php');
      $_SESSION['module'] = "login";
      // require_once(LIBS . 'twitteroauth/twitteroauth.php');
  }

  function login_sn(){
    // echo json_encode("HOLA");
    // die();
    $user = json_decode($_POST['user'], true);

    set_error_handler('ErrorHandler');
    try {
        $arrArgument = array(
            'usuario' => $user['id']
        );

        $arrValue = loadModel(MODEL_LOGIN, "login_model", "count_SN", $arrArgument);
    } catch (Exception $e) {
        $arrValue = false;
    }
    restore_error_handler();
    // echo json_encode($arrValue[0]["COUNT(*)"]);
    // die();

    if (!$arrValue[0]["COUNT(*)"]) {

        $secretKey = "NAsD98h24R";
        $issuedAt   = time();
        $notBefore  = $issuedAt + 999999;
        $data = array(
          'iat' => $issuedAt,
          'nbf' => $notBefore,
          'name' => $user['nombre'],
          'email' => $user['email']
        );
        $token = JWT::encode(
        $data,      //Data to be encoded in the JWT
        $secretKey, // The signing key
        'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
        );
        // echo json_encode($token);
        // $token = "Cha" . md5(uniqid(rand(), true));

        $arrArgument = array(
            'usuario' => $user['id'],
            'nombre' => $user['nombre'],
            'email' => $user['email'],
            'avatar' => $user['userpic'],
            'token' =>  $token,
            'tipo' => 'client',
            'activado' => "1"
        );
        // echo json_encode($arrArgument);
        // die();

        set_error_handler('ErrorHandler');
        try {
            $value = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);
        } catch (Exception $e) {
            $value = false;
        }
        restore_error_handler();
    } else
        $value = true;
      if ($value) {
          $secretKey = "NAsD98h24R";
          $issuedAt   = time();
          $notBefore  = $issuedAt + 999999;
          $data = array(
            'iat' => $issuedAt,
            'nbf' => $notBefore,
            'name' => $user['nombre'],
            'email' => $user['email']
          );
          $token = JWT::encode(
          $data,      //Data to be encoded in the JWT
          $secretKey, // The signing key
          'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
          );
          // echo json_encode($token);
          // exit();
          set_error_handler('ErrorHandler');
          $arrArgument = array(
              'username' => $user['id'],
              'token' => $token
          );
          // echo json_encode($arrArgument);
          // die();
          $user = loadModel(MODEL_LOGIN, "login_model", "update_token_login", $arrArgument);
          // echo json_encode($user);
          // exit();

          if($user){
            restore_error_handler();
            echo json_encode($token);
            exit();
          } else {
            $jsondata["success"] = false;
            $jsondata['typeErr'] = "error_server";
            echo json_encode($jsondata);
            exit;
          }
      } else {
            $jsondata["success"] = false;
            $jsondata['typeErr'] = "error";
            echo json_encode($jsondata);
            exit;
      }
  }

  function get_data(){
          // if (isset($_POST['token'])) {
              set_error_handler('ErrorHandler');
              // echo json_encode($_GET['param']);
              // echo json_encode('          ');

              $token = ereg_replace('[^ A-Za-z0-9._-]', '', $_GET['param']);
              // $token = $_GET['param'];
              // echo json_encode($token);
              // exit();

              try {
                  $arrArgument = array(
                      'token' => $token,
                  );
                  // echo json_encode($arrArgument);
                  // exit();
                  $arrValue = loadModel(MODEL_LOGIN, "login_model", "select_token", $arrArgument);
                  // echo json_encode($arrValue);
                  // die();
              } catch (Exception $e) {
                  $arrValue = false;
              }
              restore_error_handler();

              if ($arrValue) {
                  $jsondata["success"] = true;
                  $jsondata['user'] = $arrValue;
                  echo json_encode($jsondata);
                  exit();
              } else {
                  $jsondata["success"] = false;
                  $jsondata['typeErr'] = "error_server";
                  echo json_encode($jsondata);
                  exit;
              }
          // } else {
          //     $jsondata["success"] = false;
          //     $jsondata['typeErr'] = "error";
          //     echo json_encode($jsondata);
          //     exit;
          // }
  }

  function login_manual(){
        $user = $_POST;
        // echo json_encode($user);
        // die();

        $arrArgument = array(
            'username' => $user['usuario'],
            'password' => $user['pass']
        );

        set_error_handler('ErrorHandler');
        try {
            $arrValue = loadModel(MODEL_LOGIN, "login_model", "select_login", $arrArgument);
            // echo json_encode($arrValue);
            // die();
            $arrValue = password_verify($user['pass'], $arrValue[0]['password']);
            // echo json_encode($arrValue);
            // die();
        } catch (Exception $e) {
            $arrValue = false;
        }
        restore_error_handler();

        if ($arrValue !== "error") {
            if ($arrValue) { //OK
                set_error_handler('ErrorHandler');
                try {
                    $arrArgument = array(
                        'usuario' => $user['usuario'],
                        'activado' => "1"
                    );
                    // echo json_encode($arrArgument['like'][0]);
                    // die();
                    $arrValue = loadModel(MODEL_LOGIN, "login_model", "count_activado", $arrArgument);
                    // echo json_encode($arrValue);
                    // exit();
                    if ($arrValue[0]["COUNT(*)"] == 1) {
                        $secretKey = "NAsD98h24R";
                        $issuedAt   = time();
                        $notBefore  = $issuedAt + 999999;
                        $data = array(
                          'iat' => $issuedAt,
                          'nbf' => $notBefore,
                          'name' => $user['nombre'],
                          'email' => $user['email']
                        );
                        $token = JWT::encode(
                        $data,      //Data to be encoded in the JWT
                        $secretKey, // The signing key
                        'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
                        );

                        set_error_handler('ErrorHandler');
                        $arrArgument = array(
                            'username' => $user['usuario'],
                            'token' => $token
                        );
                        // echo json_encode($arrArgument);
                        // die();
                        $user = loadModel(MODEL_LOGIN, "login_model", "update_token_login", $arrArgument);

                        if($user){
                          restore_error_handler();
                          echo json_encode($token);
                          exit();
                        } else {
                          $jsondata["success"] = false;
                          $jsondata['typeErr'] = "error_server";
                          echo json_encode($jsondata);
                          exit;
                        }
                    } else {
                        $value = array(
                            "error" => true,
                            "datos" => "El usuario no ha sido activado, revise su correo"
                        );
                        echo json_encode($value);
                        exit();
                    }
                } catch (Exception $e) {
                    $value = array(
                        "error" => true,
                        "datos" => 503
                    );
                    echo json_encode($value);
                }
            } else {
                $value = array(
                    "error" => true,
                    "datos" => "El usuario y la contraseña no coinciden"
                );
                echo json_encode($value);
            }
        } else {
            $value = array(
                "error" => true,
                "datos" => 503
            );
            echo json_encode($value);
        }
  }


  function signup_user(){
    // echo json_encode("hola");
    $jsondata = array();
    // $userJSON = json_decode($_POST['signup_user_json'], true);
    $userJSON = $_POST;
    $result = validate_userPHP($userJSON);
    // echo json_encode($result);
    // die();
    if ($result['resultado']) {
      // echo json_encode("HOLA");
      // die();
        $avatar = get_gravatar($result['datos']['email'], $s = 400, $d = 'identicon', $r = 'g', $img = false, $atts = array());
        $arrArgument = array(
            'usuario' => $result['datos']['username'],
            'email' => $result['datos']['email'],
            'password' => password_hash($result['datos']['password'], PASSWORD_BCRYPT),
            'avatar' => $avatar,
            'tipo' => 'client',
            'token' => ""
        );
        // echo json_encode($arrArgument);
        // die();
        set_error_handler('ErrorHandler');
        try {
          $arrValue = loadModel(MODEL_LOGIN, "login_model", "count", $arrArgument);
          // echo json_encode($arrValue);
          // die();
          if ($arrValue[0]['COUNT(*)'] > 0) {
              $typeErr = 'Name';
              $error = "Nombre de usuario no disponible";

              $jsondata["success"] = false;
              $jsondata['typeErr'] = $typeErr;
              $jsondata["error"] = $error;
              echo json_encode($jsondata);
              exit;

          } else {
            // echo json_encode($arrArgument);
            // die();
              $arrValue = loadModel(MODEL_LOGIN, "login_model", "count_email", $arrArgument);
              // echo json_encode($arrValue[0]['COUNT(*)']);
              // die();
              if ($arrValue[0]['COUNT(*)'] > 0) {
                  $typeErr = 'Email';
                  $error = "Email ya registrado";

                  $jsondata["success"] = false;
                  $jsondata['typeErr'] = $typeErr;
                  $jsondata["error"] = $error;
                  echo json_encode($jsondata);
                  exit;
              }
            }
        } catch (Exception $e) {
            $arrValue = false;
        }

        if ($arrValue) {
            set_error_handler('ErrorHandler');
            try {
                //loadModel
                $arrArgument['token'] = "Ver" . md5(uniqid(rand(), true));
                // echo json_encode($arrArgument);
                // die();
                $arrValue = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);
                // DELETE FROM users WHERE activado = '0'
                // echo json_encode($arrValue);
                // die();
            } catch (Exception $e) {
                $arrValue = false;
            }
            restore_error_handler();

            if ($arrValue) {
                sendtoken($arrArgument, "alta");
                // $url = amigable('?module=main&function=main_view', true);
                $jsondata["success"] = true;
                // $jsondata["redirect"] = $url;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["success"] = false;
                $jsondata['typeErr'] = "error_server";
                echo json_encode($jsondata);
                exit;
            }
        } else {
            $jsondata["success"] = false;
            $jsondata['typeErr'] = "error";
            $jsondata["error"] = $result;
            echo json_encode($jsondata);
            exit;
        }
        restore_error_handler();
    } else {
        $jsondata["success"] = false;
        $jsondata['typeErr'] = "error";
        $jsondata["error"] = $result;
        echo json_encode($jsondata);
        exit;
    }
  }

  function verify() {
      if (substr($_GET['param'], 0, 3) == "Ver") {
        // echo json_encode($_GET);
        // die();
          $arrArgument = array(
              'token' => $_GET['param']
          );
// echo json_encode($arrArgument);
// die();
          set_error_handler('ErrorHandler');
          try {
              $value = loadModel(MODEL_LOGIN, "login_model", "verify", $arrArgument);
              // echo json_encode($value);
              // die();
          } catch (Exception $e) {
              $value = false;
          }
          restore_error_handler();

          if ($value) {
              $json['user'] = $user;
              $json['success'] = true;
              echo json_encode($json);
              exit();

          }
            restore_error_handler();
            echo json_encode($value);
      }
  }

// Recover
  function restore(){
      $result = array();
      if (isset($_POST['inputEmail'])) {
          $result = validatemail($_POST['inputEmail']);
          if ($result) {
              $secretKey = "NAsD98h24R";
              $issuedAt   = time();
              $notBefore  = $issuedAt + 999999;
              $data = array(
                'iat' => $issuedAt,
                'nbf' => $notBefore,
                'name' => $user['nombre'],
                'email' => $user['email']
              );
              $token = JWT::encode(
              $data,      //Data to be encoded in the JWT
              $secretKey, // The signing key
              'HS512'     // Algorithm used to sign the token, see https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40#section-3
              );

              // $token = "Cha" . md5(uniqid(rand(), true));

              $arrArgument = array(
                  'email' => $_POST['inputEmail'],
                  'token' => $token
              );
              $arrValue = loadModel(MODEL_LOGIN, "login_model", "count_email_pass", $arrArgument);
              // echo json_encode($arrValue);
              // die();
              if ($arrValue[0]['COUNT(*)'] == 1) {
                  $arrValue = loadModel(MODEL_LOGIN, "login_model", "update_token", $arrArgument);
                  // echo json_encode($arrValue);
                  // die();
                  if ($arrValue) {
                      //////////////// Envio del correo al usuario
                      $arrArgument = array(
                          'token' => $token,
                          'email' => $_POST['inputEmail']
                      );
                      if (sendtoken($arrArgument, "modificacion"))
                          echo "Tu nueva contraseña ha sido enviada al email";
                      else
                          echo "Error en el servidor. Intentelo más tarde";
                  }
              } else {
                  echo "El email introducido no existe ";
              }
          } else {
              echo "El email no es válido";
          }
      }
  }

  function update_pass() {
    // echo json_encode($_POST);
    // die();

      $arrArgument = array(
        'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
        'token' => $_POST['token']
      );
      // echo json_encode($arrArgument);
      // die();

      set_error_handler('ErrorHandler');
      try {
          $value = loadModel(MODEL_LOGIN, "login_model", "update_password", $arrArgument);
      } catch (Exception $e) {
          $value = false;
      }
      restore_error_handler();

      if ($value) {
          $jsondata["success"] = true;
          echo json_encode($jsondata);
          exit;
      } else {
          $jsondata["success"] = false;
          echo json_encode($jsondata);
          exit;
      }
  }

//profile
  function upload_avatar() {
      $result_avatar = upload_files();
      $_SESSION['avatar'] = $result_avatar;
      echo json_encode($result_avatar);
  }

  function delete_avatar() {
    // echo json_encode($_SESSION['avatar']);
      $_SESSION['avatar'] = array();
      $result = remove_files();
      if ($result === true) {
          echo json_encode(array("res" => true));
      } else {
          echo json_encode(array("res" => false));
      }
  }

  function profile_filler() {
    // echo json_encode($_GET);
    // die();
      if (isset($_GET['param'])) {
          set_error_handler('ErrorHandler');

          $token = ereg_replace('[^ A-Za-z0-9._-]', '', $_GET['param']);

          try {
              $arrArgument = array(
                  'token' => $token
              );
              // echo json_encode($arrArgument);
              // die();
              $arrValue = loadModel(MODEL_LOGIN, "login_model", "select_manual", $arrArgument);
              // echo json_encode($arrValue);
              // die();
          } catch (Exception $e) {
              $arrValue = false;
          }
          restore_error_handler();

          if ($arrValue) {
              $jsondata["success"] = true;
              if ($_GET['param'] != '%%')
                  $jsondata['user'] = $arrValue[0];
              else
                  $jsondata['user'] = $arrValue;
              echo json_encode($jsondata);
              exit();
          } else {
              $jsondata["success"] = false;
              echo json_encode($jsondata);
              exit();
          }
      } else {
          $jsondata["success"] = false;
          echo json_encode($jsondata);
          exit();
      }
  }

  function update_profile() {
      $jsondata = array();
      $userJSON = $_POST;
      // echo json_encode($userJSON);
      // die();

      $result = validate_userPHP($userJSON);
      // echo json_encode($result['datos']);
      // die();
      if ($result['resultado']) {
          $arrArgument = array(
              'name' => $userJSON['name'],
              'username' => $userJSON['username'],
              'email' => $result['datos']['email'],
              // 'password' => password_hash($result['datos']['password'], PASSWORD_BCRYPT),
              'date_birthday' => $userJSON['date_birthday'],
              'avatar' => $userJSON['avatar'],
              'country' => $userJSON['pais'],
              'league' => $userJSON['liga'],
              'team' => $userJSON['equipo']
          );
          // echo json_encode($arrArgument);
          // die();
          set_error_handler('ErrorHandler');
          try {
              $arrValue = loadModel(MODEL_LOGIN, "login_model", "update_profile", $arrArgument);
          } catch (Exception $e) {
              $arrValue = false;
          }
          restore_error_handler();
          if ($arrValue) {
              //$jsondata["success"] = true;
              //echo json_encode($jsondata);
              //exit;

              set_error_handler('ErrorHandler');
              $arrArgument = array(
                  'username' => $arrArgument['username']
              );
              // echo json_encode($arrArgument);
              // die();
              $user = loadModel(MODEL_LOGIN, "login_model", "select", $arrArgument);
              restore_error_handler();
              $jsondata["success"] = true;
              $jsondata['user'] = $user;
              echo json_encode($jsondata);
              exit();
          } else {
              $jsondata["success"] = false;
              echo json_encode($jsondata);
          }
      } else {
          $jsondata["success"] = false;
          $jsondata['datos'] = $result;
          echo json_encode($jsondata);
      }
  }

  function load_pais_user() {
      if ((isset($_GET["param"])) && ($_GET["param"] == true)) {
          $jsondata = array();
          $json = array();

          set_error_handler('ErrorHandler');
          try {
              $json = loadModel(MODEL_LOGIN, "login_model", "select_country");
              // echo json_encode($json);
              // die();
          } catch (Exception $e) {
              $json = false;
          }
          restore_error_handler();

          if ($json) {
              $jsondata["paises"] = $json;
              echo json_encode($jsondata);
              exit;
          } else {
              $jsondata["paises"] = "error";
              echo json_encode($jsondata);
              exit;
          }
      }
  }

  function load_ligas_user() {
    // echo json_encode($_POST);
    // die();
       if ($_GET['param']) {
          $jsondata = array();
          $json = array();
          // echo json_encode($_POST);
          // die();

          set_error_handler('ErrorHandler');
          try {
              $json = loadModel(MODEL_LOGIN, "login_model", "select_league", $_GET['param']);
              // echo json_encode($json);
              // die();
          } catch (Exception $e) {
              $json = false;
          }
          restore_error_handler();

          if ($json) {
              $jsondata["ligas"] = $json;
              echo json_encode($jsondata);
              exit;
          } else {
              $jsondata["ligas"] = "error";
              echo json_encode($jsondata);
              exit;
          }
      }
  }

  function load_equipos_user() {
    // echo json_encode($_GET);
    // die();
      if ($_GET['param']) {
          $jsondata = array();
          $json = array();

          set_error_handler('ErrorHandler');
          try {
              $json = loadModel(MODEL_LOGIN, "login_model", "select_team", $_GET['param']);
              // echo json_encode($json);
              // die();
          } catch (Exception $e) {
              $json = false;
          }
          restore_error_handler();

          if ($json) {
              $jsondata["equipos"] = $json;
              echo json_encode($jsondata);
              exit;
          } else {
              $jsondata["equipos"] = "error";
              echo json_encode($jsondata);
              exit;
          }
      }
  }
}
