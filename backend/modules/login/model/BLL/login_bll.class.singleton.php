<?php
// echo json_encode("BLL");
// die();
class login_bll {
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = login_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_user_BLL($arrArgument) {
        return $this->dao->create_user_DAO($this->db, $arrArgument);
    }

    public function count_BLL($arrArgument) {
        return $this->dao->count_DAO($this->db, $arrArgument);
    }

    public function count_SN_BLL($arrArgument) {
        return $this->dao->count_SN_DAO($this->db, $arrArgument);
    }

    public function update_token_login_BLL($arrArgument) {
        return $this->dao->update_token_login_DAO($this->db, $arrArgument);
    }

    public function count_activado_BLL($arrArgument) {
        return $this->dao->count_activado_DAO($this->db, $arrArgument);
    }

    public function count_email_BLL($arrArgument) {
        return $this->dao->count_email_DAO($this->db, $arrArgument);
    }

    public function count_email_pass_BLL($arrArgument) {
        return $this->dao->count_email_pass_DAO($this->db, $arrArgument);
    }

    public function select_BLL($arrArgument) {
        return $this->dao->select_DAO($this->db, $arrArgument);
    }

    public function select_token_BLL($arrArgument) {
        return $this->dao->select_token_DAO($this->db, $arrArgument);
    }

    public function select_manual_BLL($arrArgument) {
        return $this->dao->select_manual_DAO($this->db, $arrArgument);
    }

    public function select_login_BLL($arrArgument) {
        return $this->dao->select_login_DAO($this->db, $arrArgument);
    }

    public function verify_BLL($arrArgument) {
        return $this->dao->verify_DAO($this->db, $arrArgument);
    }

    public function update_token_BLL($arrArgument) {
        return $this->dao->update_token_DAO($this->db, $arrArgument);
    }

    public function update_password_BLL($arrArgument) {
        return $this->dao->update_password_DAO($this->db, $arrArgument);
    }

    public function update_profile_BLL($arrArgument) {
        return $this->dao->update_profile_DAO($this->db, $arrArgument);
    }

    public function select_country_BLL($arrArgument) {
        return $this->dao->select_country_DAO($this->db, $arrArgument);
    }

    public function select_league_BLL($arrArgument) {
        return $this->dao->select_league_DAO($this->db, $arrArgument);
    }

    public function select_team_BLL($arrArgument) {
        return $this->dao->select_team_DAO($this->db, $arrArgument);
    }

}
