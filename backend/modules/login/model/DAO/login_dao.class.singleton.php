<?php
// echo json_encode("DAO");
// die();
class login_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function count_DAO($db, $arrArgument) {
      // echo json_encode($arrArgument);
      // die();
      $id = $arrArgument['usuario'];
      // echo json_encode($id);
      // die();
      $sql = "SELECT COUNT(*) FROM users WHERE username LIKE '$id' AND name = ''";
      $stmt = $db->ejecutar($sql);
      return $db->listar($stmt);
    }

    public function select_token_DAO($db, $arrArgument){
      $token = $arrArgument['token'];

      $sql = "SELECT * FROM users WHERE token_get LIKE '$token'";
      // echo json_encode($sql);
      // die();

      $stmt = $db->ejecutar($sql);
      return $db->listar($stmt);
    }

    public function count_SN_DAO($db, $arrArgument) {
      // echo json_encode($arrArgument);
      // die();
      $id = $arrArgument['usuario'];
      // echo json_encode($id);
      // die();
      $sql = "SELECT COUNT(*) FROM users WHERE username LIKE '$id'";
      $stmt = $db->ejecutar($sql);
      return $db->listar($stmt);
    }

    public function update_token_login_DAO($db, $arrArgument) {
        // echo json_encode($arrArgument);
        // die();
        $token = $arrArgument['token'];
        $username = $arrArgument['username'];

        $sql = "UPDATE users SET token_get = '$token' WHERE username = '$username'";
        // echo json_encode($sql);
        // die();

        // $stmt = $db->ejecutar($sql);
        // return $db->listar($stmt);

        return $db->ejecutar($sql);
    }

    public function count_activado_DAO($db, $arrArgument) {
      // echo json_encode($arrArgument);
      // die();
      $id = $arrArgument['usuario'];
      // echo json_encode($id);
      // die();
      $sql = "SELECT COUNT(*) FROM users WHERE username LIKE '$id' AND activado = '1'";
      // echo json_encode($sql);
      // die();
      $stmt = $db->ejecutar($sql);
      return $db->listar($stmt);
    }

    public function count_email_pass_DAO($db, $arrArgument) {
      // echo json_encode($arrArgument);
      // die();
      $email = $arrArgument['email'];
      // echo json_encode($email);
      // die();
      $sql = "SELECT COUNT(*) FROM users WHERE email LIKE '$email'";
      $stmt = $db->ejecutar($sql);
      return $db->listar($stmt);
    }

    public function count_email_DAO($db, $arrArgument) {
      // echo json_encode($arrArgument);
      // die();
      $email = $arrArgument['email'];
      // echo json_encode($email);
      // die();
      $sql = "SELECT COUNT(*) FROM users WHERE email LIKE '$email' AND token NOT LIKE ''";
      // echo json_encode($sql);
      // die();
      $stmt = $db->ejecutar($sql);
      return $db->listar($stmt);
    }

    public function create_user_DAO($db,$arrArgument) {
      // echo json_encode($arrArgument);
      // die();
      $usuario = $arrArgument['usuario'];
      $nombre = $arrArgument['nombre'];
      $email = $arrArgument['email'];
      $password = $arrArgument['password'];
      $tipo = $arrArgument['tipo'];
      $token = $arrArgument['token'];
      $userpic = $arrArgument['avatar'];
      if ($arrArgument['activado'])
          $activado = $arrArgument['activado'];
      else
          $activado = 0;

      $sql = "INSERT INTO users (username, name, email, password, userpic, tipo, activado, token) VALUES ('$usuario', '$nombre', '$email',"
              . " '$password', '$userpic', '$tipo', '$activado','$token')";

      return $db->ejecutar($sql);
      return $db->listar($stmt);
    }

    public function select_DAO($db, $arrArgument) {
      $id = $arrArgument['username'];
      // echo json_encode($arrArgument['like'][0]);
      // die();
      $sql = "SELECT * FROM users WHERE username LIKE '$id'";
      // echo json_encode($sql);

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function verify_DAO($db,$arrArgument){
      // echo json_encode($arrArgument);
      // die();
      $token = $arrArgument['token'];

      $sql = "UPDATE users SET activado = '1' WHERE token LIKE '$token'";
      // echo json_encode($sql);
      // die();

      return $db->ejecutar($sql);
      // $stmt = $db->ejecutar($sql);
      // return $db->listar($stmt);
    }

    public function select_manual_DAO($db, $arrArgument) {
      $id = $arrArgument['token'];
      // echo json_encode($id);
      // die();
      $sql = "SELECT * FROM users WHERE token_get LIKE '$id'";
      // echo json_encode($sql);
      // die();

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function select_login_DAO($db, $arrArgument) {
      $username = $arrArgument['username'];
      // echo json_encode($id);
      // die();
      $sql = "SELECT * FROM users WHERE username LIKE '$username'";
      // echo json_encode($sql);
      // die();

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function update_token_DAO($db,$arrArgument){
      // echo json_encode($arrArgument);
      // die();
      $token = $arrArgument['token'];
      $email = $arrArgument['email'];

      $sql = "UPDATE users SET token = '$token' WHERE email = '$email'";
      // echo json_encode($sql);
      // die();

      // $stmt = $db->ejecutar($sql);
      // return $db->listar($stmt);

      return $db->ejecutar($sql);
    }

    public function update_password_DAO($db, $arrArgument){
      $token = $arrArgument['token'];
      $password = $arrArgument['password'];

      $sql = "UPDATE users SET password = '$password' WHERE token = '$token'";
      // echo json_encode($sql);
      // die();

      return $db->ejecutar($sql);
    }

    public function update_profile_DAO($db, $arrArgument){
      $name = $arrArgument['name'];
      $username = $arrArgument['username'];
      $date_birthday = $arrArgument['date_birthday'];
      $email = $arrArgument['email'];
      $userpic = $arrArgument['avatar'];
      // $password = $arrArgument['password'];
      $league = $arrArgument['league'];
      $country = $arrArgument['country'];
      $team = $arrArgument['team'];

      $sql = "UPDATE users SET name = '$name', username = '$username', birth_date = '$date_birthday', userpic = '$userpic'
                -- , password = '$password'
                , league = '$league', country = '$country'
                , team = '$team' WHERE email = '$email' AND token NOT LIKE ''";
      // echo json_encode($sql);
      // die();

      return $db->ejecutar($sql);
    }

    public function select_country_DAO($db) {
        $sql = "SELECT * FROM countries";
      // echo json_encode($sql);
      // die();
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function select_league_DAO($db, $arrArgument) {
        $sql = "SELECT * FROM leagues WHERE id_country LIKE '$arrArgument'";
      // echo json_encode($sql);
      // die();
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function select_team_DAO($db, $arrArgument) {
        $sql = "SELECT * FROM teams WHERE id_league LIKE '$arrArgument'";
      // echo json_encode($sql);
      // die();
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

}
