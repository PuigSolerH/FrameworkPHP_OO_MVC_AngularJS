<?php
// echo json_encode("MODEL");
// die();
class login_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = login_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_user($arrArgument) {
        return $this->bll->create_user_BLL($arrArgument);
    }

    public function count($arrArgument) {
        return $this->bll->count_BLL($arrArgument);
    }

    public function count_SN($arrArgument) {
        return $this->bll->count_SN_BLL($arrArgument);
    }

    public function update_token_login($arrArgument) {
        return $this->bll->update_token_login_BLL($arrArgument);
    }

    public function count_email_pass($arrArgument) {
        return $this->bll->count_email_pass_BLL($arrArgument);
    }

    public function count_activado($arrArgument) {
        return $this->bll->count_activado_BLL($arrArgument);
    }

    public function count_email($arrArgument) {
        return $this->bll->count_email_BLL($arrArgument);
    }

    public function select($arrArgument) {
        return $this->bll->select_BLL($arrArgument);
    }

    public function select_token($arrArgument) {
        return $this->bll->select_token_BLL($arrArgument);
    }

    public function select_manual($arrArgument) {
        return $this->bll->select_manual_BLL($arrArgument);
    }

    public function select_login($arrArgument) {
        return $this->bll->select_login_BLL($arrArgument);
    }

    public function verify($arrArgument) {
        return $this->bll->verify_BLL($arrArgument);
    }

    public function update_token($arrArgument) {
        return $this->bll->update_token_BLL($arrArgument);
    }

    public function update_password($arrArgument) {
        return $this->bll->update_password_BLL($arrArgument);
    }

    public function update_profile($arrArgument) {
        return $this->bll->update_profile_BLL($arrArgument);
    }

    public function select_country($arrArgument) {
        return $this->bll->select_country_BLL($arrArgument);
    }

    public function select_team($arrArgument) {
        return $this->bll->select_team_BLL($arrArgument);
    }

    public function select_league($arrArgument) {
        return $this->bll->select_league_BLL($arrArgument);
    }
}
