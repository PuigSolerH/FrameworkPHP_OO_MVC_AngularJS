# FW_PHP_OO_MVC_AngularJS made in 1DAW

This application made in programming class contains the following modules:

-HOME:
  The HOME offers a carousel made by an AngularJS directive and the categories about the players to filter them by one or another one. The categories lists has a 'load more' button to load categories 2 by 2 and an autocomplete for filter it by writting the name.

-PLAYERS:
  The PLAYERS module offers a maps where there are some markers which possition has been obtained through database info and when you click on one you can see some info about one players. There is a link on each one to go to the details about the players to see more information about him.

  Under it, the application presents a list with all the players with a brief information and a link to go to the details section.
  The players are filtered by 2 ways:
    -Autocomplete: There's an autocomplete where you can filter the players by writting the name of them.
    -Pagination: This is a way for filter players because you can only see only 3 on each page so it's a way to not have all of them in the same window.

  Details: You can access them by clicking on the links said before and it presents a window where you can see all the information about the player selected and a map with, specifically, the location of him.

    -Like: On each player's details, it the user has been logged in, he can add to his Likes List the players he want. He can also too delete them. The user can see the players added to Likes List on the 'Fvorite Players' menu option.

    -Comment: The users logged in can make comments on each player details' section and he can also see all the comment on each player.

-CONTACT:
  The contact module offers the users to send an email. It was only made for checking if all about the 'send emails' was successfully.

-LOGIN:
  This module presents a window where you can login manually with a username and a password and some buttons for the following utilities:

    -Sign Up: The sign up button redirects the user to a window where he can input a Username, an Email and a Password.
      Once it's done an email will sent to the email written where the user can verify the register.

    -Recover Password: The recover password button redirects the user to a window where he can write an email address and a email will be sent to that email addres.
      Once the user clicks on the link offered on it, it will redirect to a page where the user can write the new password.

    -Social Signin:
      The user can Log In the application by using 3 Social Networks: Google+, Facebook and Twitter.
      It's made by using Firebase with it's own services.js.

-PROFILE:
  Once the user has logged in, the profile module will be accessible.
  On this function the user can see all the information about him that whe have saved on the database and he can modify some of them, for example, the profile pic, the birth date...
  This window has 3 dropdowns where the user can choose his favourite team.
  It presents too a datepicker for select the Birthday Date.
  Also it has a dropzone where the user can upload a new profile pic.

There are some details but there is not much to comment about it, it will be better seen with the code.

## Made by Hibernon Puig Soler
